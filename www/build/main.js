webpackJsonp([0],{

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__visit_visit__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(289);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabsPage = /** @class */ (function () {
    function TabsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tab1 = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.tab2 = __WEBPACK_IMPORTED_MODULE_3__profile_profile__["a" /* ProfilePage */];
        this.tab3 = __WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */];
        this.tab4 = __WEBPACK_IMPORTED_MODULE_4__visit_visit__["a" /* VisitPage */];
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad TabsPage');
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-tabs',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/tabs/tabs.html"*/'<ion-tabs #myTabs>\n  <ion-tab [root]="tab1" tabTitle="Home" tabIcon="ico-homep"></ion-tab>\n  <ion-tab [root]="tab2" tabTitle="Perfil" tabIcon="ico-perfil"></ion-tab>\n  <ion-tab [root]="tab3" tabTitle="Compartir" tabIcon="ico-comp"></ion-tab>\n  <ion-tab [root]="tab4" tabTitle="Tienda" tabIcon="ico-tienda"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/tabs/tabs.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]) === "function" && _b || Object])
    ], TabsPage);
    return TabsPage;
    var _a, _b;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 117:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 117;

/***/ }),

/***/ 159:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 159;

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cloth_cloth__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_url_services__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, http, loading) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.loading = loading;
        this.items = [];
        this.timer = 0;
        this.timerSeg = 0;
        this.timerMin = 0;
        //  this.initialItems();
        this.current = 5;
        this.max = 30;
        //this.startTimer();
        this.getTypes();
        this.load = this.loading.create({
            content: "Cargando ..."
        });
        this.load.present();
    }
    HomePage.prototype.initialItems = function () {
        this.items = [
            {
                name: 'Hombre'
            },
            {
                name: 'Mujer'
            },
            {
                name: 'Niño'
            },
            {
                name: 'Niña'
            }
        ];
    };
    HomePage.prototype.startTimer = function () {
        var intervalVar = setInterval(function () {
            //alert(this.timer++)
            if (this.timer == this.max) {
                alert('se cumplio el tiempoooooo!!');
                clearInterval(intervalVar);
            }
            else {
                if (this.timerSeg == 60) {
                    this.timerMin++;
                    this.timerSeg = 0;
                }
                else {
                    this.timer++;
                    this.timerSeg++;
                }
            }
        }.bind(this), 1000);
    };
    HomePage.prototype.sendCloth = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__cloth_cloth__["a" /* ClothPage */], { use: id });
    };
    HomePage.prototype.getTypes = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_4__config_url_services__["a" /* URL_SERVICES */] + "/api/customer/uses";
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["c" /* HttpHeaders */]();
        headers = headers.append('Content-Type', 'application/json');
        //headers= headers.append('Authorization', 'Bearer '+data);
        this.http.get(url, { headers: headers }).subscribe(function (response) {
            //this.restaurants = response;
            _this.items = response;
            console.log(response);
            _this.load.dismiss().then(function () {
                console.log('response Success');
            });
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar style="text-align: center;" color="dark">\n    <ion-title>\n      CHIC MUA\n    </ion-title>\n    <div style="position: relative;">\n      <div class="progress-percentage-wrapper timer">\n        <div class="progress-percentage ng-cloak">{{timerMin +\':\'+ timerSeg}}</div>\n        <round-progress [max]="max"\n                        [current]="timer"\n                        [color]="\'#45ccce\'"\n                        [background]="\'#eaeaea\'"\n                        [radius]="100"\n                        [stroke]="20"\n                        [semicircle]="false"\n                        [rounded]="true"\n                        [clockwise]="true"\n                        [responsive]="false"\n                        [duration]="1000"\n                        [animation]="\'easeInOutQuart\'"\n                        [animationDelay]="100" class="round"></round-progress>\n      </div>\n    </div>\n  </ion-navbar>\n</ion-header>\n<div class="test">\n  <img  src="../../assets/imgs/promo-del-mes.png">\n</div>\n<ion-content padding class="page-home">\n  <ion-grid>\n    <!--<ion-row>\n      <ion-col>\n        <img src="../assets/imgs/banner.jpg">\n      </ion-col>\n    </ion-row>\n    <!--<ion-row>\n      <ion-col col-6 *ngFor="let item of items" style="text-align:center;">\n        <ion-card style="height: 100px;" (click)="sendCloth(item.id)">\n          <p>{{item.name}}</p>\n        </ion-card>\n      </ion-col>\n    </ion-row>-->\n    <ion-row>\n      <ion-col>\n        <p class="p-home">ARMA</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <p class="p-home">TU</p>\n      </ion-col>\n      <ion-col col-6>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <p class="p-home">PRENDA</p>\n      </ion-col>\n    </ion-row>\n    <br>\n    <ion-row>\n      <ion-col col-3>\n        <div class="div-one">\n          <span class="span-one">1</span>\n        </div>\n      </ion-col>\n      <ion-col col-9>\n        <p class="p-select">ELIGE:</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col *ngFor="let item of items" (click)="sendCloth(item.id)">\n        <img *ngIf="item.name == \'Hombre\'" class="img-fish" src="../../assets/imgs/hombre.png">\n        <p class="price" *ngIf="item.name == \'Hombre\'">$599</p>\n        <img *ngIf="item.name == \'Mujer\'" class="img-fish" src="../../assets/imgs/mujer.png">\n        <p class="price" *ngIf="item.name == \'Mujer\'">$599</p>\n        <img *ngIf="item.name == \'Niño\'"  class="img-fish" src="../../assets/imgs/nino.png">\n        <p class="price" *ngIf="item.name == \'Niño\'">$499</p>\n        <img *ngIf="item.name == \'Niña\'" class="img-fish" src="../../assets/imgs/nina.png">\n        <p class="price" *ngIf="item.name == \'Niña\'">$499</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClothPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_models__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_url_services__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ClothPage = /** @class */ (function () {
    function ClothPage(navCtrl, navParams, http, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loading = loading;
        this.items = [];
        //this.initialItems();
        this.use = this.navParams.get("use");
        console.log(this.js);
        this.load = this.loading.create({
            content: 'Cargando...'
        });
        this.load.present();
    }
    ClothPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ClothPage');
        this.getPatterns();
    };
    ClothPage.prototype.getPatterns = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_4__config_url_services__["a" /* URL_SERVICES */] + "/api/customer/patterns";
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["c" /* HttpHeaders */]();
        headers = headers.append('Content-Type', 'application/json');
        //headers= headers.append('Authorization', 'Bearer '+data);
        this.http.get(url, { headers: headers }).subscribe(function (response) {
            //this.restaurants = response;
            _this.items = response;
            _this.load.dismiss().then(function () {
                console.log('response success');
            });
            console.log(response);
        });
    };
    ClothPage.prototype.initialItems = function () {
        this.items = [
            {
                name: 'Patron unicornio',
                piece: '10m',
                charge: 100,
                ofer: '10%'
            },
            {
                name: 'Patron de papaya',
                piece: '10m',
                charge: 0,
                ofer: '5%'
            },
            {
                name: 'Patron de sandia',
                piece: '10m',
                charge: 100,
                ofer: '50%'
            },
            {
                name: 'Patron de pera',
                piece: '10m',
                charge: 40,
                ofer: '15%'
            }
        ];
    };
    ClothPage.prototype.sendModel = function (pattern) {
        this.js = {
            use: this.use,
            pattern: pattern
        };
        console.log(this.js);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__models_models__["a" /* ModelsPage */], { data: this.js });
    };
    ClothPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-cloth',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/cloth/cloth.html"*/'<!--\n  Generated template for the ClothPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="light">\n    <ion-title>CHIC MUA</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<!--<div class="test">\n  <img  src="../../assets/imgs/escarabajo.png">\n</div>-->\n<ion-content padding>\n<ion-grid>\n  <ion-row>\n    <ion-col col-3>\n      <div class="div-two">\n        <span class="span-two">2</span>\n      </div>\n    </ion-col>\n    <ion-col col-9>\n      <p class="p-select-two">ELIGE TU TELA:</p>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <p class="p-cloth">TELAS DE TEMPORADA</p>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col *ngFor="let item of items" (click)="sendModel(item.id)" col-6>\n      <img class="img-cloth" src="{{item.picture}}">\n      <p>Piezas {{item.yardage}}m</p>\n      <p *ngIf="item.increment != 0">Cargo extra: {{item.increment}}</p>\n      <div *ngFor="let offer of item.offers">\n        <p>Oferta: {{offer.offer.percent}}%</p>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/cloth/cloth.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
    ], ClothPage);
    return ClothPage;
}());

//# sourceMappingURL=cloth.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModelsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_url_services__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__review_model_review_model__ = __webpack_require__(205);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ModelsPage = /** @class */ (function () {
    function ModelsPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.segment = 'all';
        this.js = this.navParams.get("data");
    }
    ModelsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModelsPage');
        this.initialItems();
        this.getModels();
    };
    ModelsPage.prototype.initialItems = function () {
        this.items = [
            {
                name: 'Patron unicornio',
                piece: '10m',
                charge: 100,
                ofer: '10%'
            },
            {
                name: 'Patron de papaya',
                piece: '10m',
                charge: 0,
                ofer: '5%'
            },
            {
                name: 'Patron de sandia',
                piece: '10m',
                charge: 100,
                ofer: '50%'
            },
            {
                name: 'Patron de pera',
                piece: '10m',
                charge: 40,
                ofer: '15%'
            },
            {
                name: 'Patron unicornio',
                piece: '10m',
                charge: 100,
                ofer: '10%'
            },
            {
                name: 'Patron de papaya',
                piece: '10m',
                charge: 0,
                ofer: '5%'
            },
            {
                name: 'Patron de sandia',
                piece: '10m',
                charge: 100,
                ofer: '50%'
            },
            {
                name: 'Patron de pera',
                piece: '10m',
                charge: 40,
                ofer: '15%'
            },
            {
                name: 'Patron de pera',
                piece: '10m',
                charge: 40,
                ofer: '15%'
            }
        ];
    };
    ModelsPage.prototype.getModels = function () {
        console.log(this.js);
        var url = __WEBPACK_IMPORTED_MODULE_3__config_url_services__["a" /* URL_SERVICES */] + "/api/customer/models";
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]();
        headers = headers.append('Content-Type', 'application/json');
        //headers= headers.append('Authorization', 'Bearer '+data);
        this.http.post(url, this.js, { headers: headers }).subscribe(function (response) {
            //this.restaurants = response;
            //this.items = response;
            console.log(response);
        });
    };
    ModelsPage.prototype.sendReview = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__review_model_review_model__["a" /* ReviewModelPage */]);
    };
    ModelsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-models',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/models/models.html"*/'<!--\n  Generated template for the ModelsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="light">\n    <ion-title>CHIC MUA</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-row>\n    <ion-col col-3>\n      <div class="div-two">\n        <span class="span-two">3</span>\n      </div>\n    </ion-col>\n    <ion-col col-9>\n      <p class="p-select-two">ELIGE</p>\n      <p class="p-select-two">MODELO</p>\n    </ion-col>\n  </ion-row>\n  <div style="position: relative;">\n    <div class="test-car">\n      <img style="width:65%;" src="../../assets/imgs/carrito.png">\n      <p class="p-checkout">1</p>\n    </div>\n    <div class="test">\n      <img style="width:70%;" src="../../assets/imgs/escarabajo-3.png">\n    </div>\n  </div>\n  <div no-border class="scrollable-segments segments-bord">\n    <!--<p class="models">Modelos</p>-->\n    <ion-segment [(ngModel)]="segment">\n      <ion-segment-button value="all">\n        Todos\n      </ion-segment-button>\n      <ion-segment-button value="bikini">\n        Bikinis\n      </ion-segment-button>\n      <ion-segment-button value="swimwear">\n        Trajes de Baño\n      </ion-segment-button>\n\n    </ion-segment>\n  </div>\n  <div [ngSwitch]="segment">\n    <ion-list *ngSwitchCase="\'all\'">\n      <ion-grid>\n        <ion-row>\n          <ion-col *ngFor="let item of items" col-4  (click)="sendReview()">\n            <img src="../assets/imgs/capri.png">\n            <p class="p-describe">CAPRI</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-list>\n    <ion-list *ngSwitchCase="\'bikini\'">\n      <ion-grid>\n        <ion-row>\n          <ion-col *ngFor="let item of items" col-4  (click)="sendReview()">\n            <img src="../assets/imgs/tania.png">\n            <p class="p-describe">TANIA</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-list>\n    <ion-list *ngSwitchCase="\'swimwear\'">\n      <ion-grid>\n        <ion-row>\n          <ion-col *ngFor="let item of items" col-4  (click)="sendReview()">\n            <img src="../assets/imgs/trikini-v.png">\n            <p class="p-describe">TRIKINI V</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-list>\n  </div>\n</ion-content>\n<ion-footer>\n  <ion-toolbar class="toolbar-footer">\n      <img src="../../assets/imgs/elige-modelo.jpg">\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/models/models.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], ModelsPage);
    return ModelsPage;
}());

//# sourceMappingURL=models.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReviewModelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__size_size__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReviewModelPage = /** @class */ (function () {
    function ReviewModelPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
    }
    ReviewModelPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReviewModelPage');
    };
    ReviewModelPage.prototype.sendNext = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: '¿Desea copas?',
            message: '$70 pesos',
            buttons: [
                {
                    text: 'Si',
                    role: 'cancel',
                    handler: function () {
                        //console.log('Cancel clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__size_size__["a" /* SizePage */]);
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        //console.log('Buy clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__size_size__["a" /* SizePage */]);
                    }
                }
            ]
        });
        alert.present();
    };
    ReviewModelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-review-model',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/review-model/review-model.html"*/'<!--\n  Generated template for the ReviewModelPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="light">\n    <ion-title>CHIC MUA</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-row>\n    <div style="">\n      <img style="position: absolute;" src="../../assets/imgs/patron.jpeg">\n      <img style="position: absolute;" src="../../assets/imgs/delantero_clasico.png">\n    </div>\n  </ion-row>\n</ion-content>\n<ion-footer>\n  <ion-row class="row-button">\n    <ion-col style="text-align: center;">\n      <button class="button-save" ion-button type="button" (click)="sendNext()">Seleccionar</button>\n    </ion-col>\n  </ion-row>\n</ion-footer>'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/review-model/review-model.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ReviewModelPage);
    return ReviewModelPage;
}());

//# sourceMappingURL=review-model.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SizePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_url_services__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__help_help__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__add_other_add_other__ = __webpack_require__(208);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SizePage = /** @class */ (function () {
    function SizePage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        //this.getSizes();
        this.initialItems();
    }
    SizePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SizePage');
        //this.getSizes();
    };
    SizePage.prototype.getSizes = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_3__config_url_services__["a" /* URL_SERVICES */] + "/api/customer/sizes";
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]();
        headers = headers.append('Content-Type', 'application/json');
        //headers= headers.append('Authorization', 'Bearer '+data);
        this.http.get(url, { headers: headers }).subscribe(function (response) {
            //this.restaurants = response;
            _this.items = response;
            console.log(response);
        });
    };
    SizePage.prototype.initialItems = function () {
        this.items = [
            {
                name: 'Extra chico',
                sur: 'XS',
                id: 1,
                img: "../../assets/imgs/xs.png"
            },
            {
                name: 'Chico',
                sur: 'S',
                id: 2,
                img: "../../assets/imgs/s.png"
            },
            {
                name: 'Mediano',
                sur: 'M',
                id: 3,
                img: "../../assets/imgs/m.png"
            },
            {
                name: 'Grande',
                sur: 'L',
                id: 4,
                img: "../../assets/imgs/l.png"
            },
            {
                name: 'Extra Grande',
                sur: 'XL',
                id: 5,
                img: "../../assets/imgs/xl.png"
            },
            {
                name: 'Extra Extra Grande',
                sur: '2XL',
                id: 6,
                img: "../../assets/imgs/2xl.png"
            },
            {
                name: 'Extra Extra Extra Grande',
                sur: '3XL',
                id: 7,
                img: "../../assets/imgs/3xl.png"
            }
        ];
    };
    SizePage.prototype.sendHelp = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__help_help__["a" /* HelpPage */]);
    };
    SizePage.prototype.sendOther = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__add_other_add_other__["a" /* AddOtherPage */]);
    };
    SizePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-size',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/size/size.html"*/'<!--\n  Generated template for the SizePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="light">\n    <ion-title>Tallas</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="size-page" padding>\n  <ion-row>\n    <ion-col col-3>\n      <div class="div-two">\n        <span class="span-two">4</span>\n      </div>\n    </ion-col>\n    <ion-col col-9>\n      <p class="p-select-two">ELIGE</p>\n      <p class="p-select-two">TALLA</p>\n    </ion-col>\n  </ion-row>\n  <div style="position: relative;">\n    <div class="test-car">\n      <img style="width:65%;" src="../../assets/imgs/carrito.png">\n      <p class="p-checkout">1</p>\n    </div>\n  </div>\n<ion-grid>\n  <ion-row>\n    <!--<ion-col *ngFor="let item of items" col-6 no-padding>\n      <ion-card>\n        <ion-row>\n          <p>{{item.description}}</p>\n        </ion-row>\n      </ion-card>\n    </ion-col>-->\n    <ion-col style="text-align: center;" *ngFor="let item of items" col-6 no-padding>\n      <img style="width: 65%;" src="{{item.img}}" (click)="sendOther()">\n    </ion-col>\n    <ion-col>\n      <img src="../../assets/imgs/ayuda.png" (click)="sendHelp()">\n      <p class="p-help">AYUDA?</p>\n    </ion-col>\n  </ion-row>\n  <!--<ion-row>\n    <button ion-button type="button" (click)="sendHelp()">Ayuda</button>\n  </ion-row>-->\n</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/size/size.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], SizePage);
    return SizePage;
}());

//# sourceMappingURL=size.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_url_services__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HelpPage = /** @class */ (function () {
    function HelpPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.res = [];
    }
    HelpPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HelpPage');
        this.getHelp();
    };
    HelpPage.prototype.getHelp = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_3__config_url_services__["a" /* URL_SERVICES */] + "/api/customer/help";
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]();
        headers = headers.append('Content-Type', 'application/json');
        //headers= headers.append('Authorization', 'Bearer '+data);
        this.http.get(url, { headers: headers }).subscribe(function (response) {
            //this.restaurants = response;
            //this.items = response;
            _this.res = response;
            console.log(_this.res);
        });
    };
    HelpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-help',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/help/help.html"*/'<!--\n  Generated template for the HelpPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="light">\n    <ion-title>Chic Mua</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div *ngFor="let item of res">\n    <img src="{{item.picture}}">\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/help/help.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], HelpPage);
    return HelpPage;
}());

//# sourceMappingURL=help.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddOtherPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddOtherPage = /** @class */ (function () {
    function AddOtherPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AddOtherPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddOtherPage');
    };
    AddOtherPage.prototype.addOther = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
    };
    AddOtherPage.prototype.checkout = function () {
        console.log("enviar a checkout");
    };
    AddOtherPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-other',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/add-other/add-other.html"*/'<!--\n  Generated template for the AddOtherPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="light">\n    <ion-title>CHIC MUA</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="other-page" padding>\n<ion-grid>\n  <ion-row>\n    <ion-col class="cols" col-12>\n      <img class="img-button" src="../../assets/imgs/agregar-otro.png" (click)="addOther()">\n    </ion-col>\n    <ion-col class="cols" col-12>\n      <img class="img-button" src="../../assets/imgs/pagar.png" (click)="checkout()">\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/add-other/add-other.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], AddOtherPage);
    return AddOtherPage;
}());

//# sourceMappingURL=add-other.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.form = this.formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            firstname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            secondname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            birthdate: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            genre: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            email: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].email],
            phone: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            lada: ['',]
        });
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/profile/profile.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="light">\n    <ion-title>CHIC MUA </ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <div class="row-profile">\n      <ion-row style="text-align: center;">\n        <ion-col>\n          <p>Los campos marcados con * son obligatorios.</p>\n        </ion-col>\n      </ion-row>\n    </div>\n    <form [formGroup]="form" (ngSubmit)="logForm()">\n      <ion-row>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label floating>*Nombre</ion-label>\n            <ion-input type="text" [(ngModel)]="name" formControlName="name"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label floating>*Apellido Paterno</ion-label>\n            <ion-input type="text" [(ngModel)]="firstname" formControlName="firstname"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label floating>*Apellido Materno</ion-label>\n            <ion-input type="text" [(ngModel)]="secondname" formControlName="secondname"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item class="item-gender">\n            <p class="gender">Genero</p>\n            <ion-segment  [(ngModel)]="genre" formControlName="genre" color="success">\n              <ion-segment-button value="2">\n                Femenino\n              </ion-segment-button>\n              <ion-segment-button value="1">\n                Masculino\n              </ion-segment-button>\n            </ion-segment>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label floating>Fecha de nacimiento</ion-label>\n            <ion-datetime  [(ngModel)]="birthday" formControlName="birthdate" displayFormat="DD-MM-YYYY" cancelText="Cancelar" doneText="Hecho"></ion-datetime>\n          </ion-item>\n        </ion-col>\n        <br>\n        <br>\n        <br>\n        <br>\n        <!--<ion-col col-12\n          <ion-item class="item-gender">\n            <p class="gender">Genero</p>\n            <ion-segment  [(ngModel)]="gender" formControlName="genre" color="success">\n              <ion-segment-button value="2">\n                Femenino\n              </ion-segment-button>\n              <ion-segment-button value="1">\n                Masculino\n              </ion-segment-button>\n            </ion-segment>\n          </ion-item>\n        </ion-col>-->\n      </ion-row>\n      <!--<ion-row>\n        <ion-col style="text-align: center;">\n          <button class="button-register save" color="secondary" ion-button type="submit">Guardar</button>\n        </ion-col>\n      </ion-row>-->\n    </form>\n    <ion-row>\n      <ion-col col-6>\n        <p class="p-subtitle">Tarjetas</p>\n      </ion-col>\n      <ion-col col-6>\n        <button class="button-register " ion-button type="button" (click)="sendAddpayment()">Nuevo</button>\n      </ion-col>\n      <ion-col col-12>\n        <ion-list>\n          <button ion-item class="item-ios button-card" *ngFor="let item of cards" (click)="itemSelected(item)">\n            <ion-icon name="card"></ion-icon>\n            {{ item.card }}\n          </button>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <p class="p-subtitle">Direcciones</p>\n      </ion-col>\n      <ion-col col-6>\n        <button class="button-register " ion-button type="button" (click)="sendAddpayment()">Nuevo</button>\n      </ion-col>\n      <ion-col col-12>\n        <ion-list>\n          <button ion-item class="item-ios button-card" *ngFor="let item of cards" (click)="itemSelected(item)">\n            <ion-icon name="card"></ion-icon>\n            {{ item.card }}\n          </button>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col style="text-align: center;">\n        <button class="button-save" ion-button type="button"> GUARDAR </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VisitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var VisitPage = /** @class */ (function () {
    function VisitPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    VisitPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VisitPage');
    };
    VisitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-visit',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/visit/visit.html"*/'<!--\n  Generated template for the VisitPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="light">\n    <ion-title>CHIC MUA</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<ion-grid>\n  <ion-row>\n    <ion-col col-12>\n      <p class="p-home">VISIT<span class=" p-home p-home-sub">ANOS</span></p>\n    </ion-col>\n    <br>\n  </ion-row>\n  <ion-row>\n    <ion-col col-12>\n      <img  class="img-map" src="https://maps.googleapis.com/maps/api/staticmap?size=400x250&markers=19.3399383,-99.2059232&zoom=16&key=AIzaSyCztCuGrBdTescmdGtjZBFVvGTD9xps8eY"/>\n      <p style="font-size: 16px;">Durango 25 Col.Progreso Tizapán, México, CDMX</p>\n    </ion-col>\n  </ion-row>\n  <ion-row class="row-visit">\n    <ion-col class="col-hours" col-2>\n      <ion-icon name="ico-clock"></ion-icon>\n    </ion-col>\n    <ion-col col-10>\n      <p style="font-size: 16px;">Lunes a sábado de 10am a 7:30 pm y los domingos de 11 am a 6pm</p>\n    </ion-col>\n  </ion-row>\n  <ion-row class="row-visit">\n    <ion-col class="col-hours" col-2>\n      <ion-icon name="ico-phone"></ion-icon>\n    </ion-col>\n    <ion-col col-10>\n      <p style="font-size: 16px;">54562098</p>\n    </ion-col>\n  </ion-row>\n  <ion-row class="row-visit">\n    <ion-col class="col-hours" col-2>\n      <ion-icon name="ico-whats"></ion-icon>\n    </ion-col>\n    <ion-col col-10>\n      <p style="font-size: 16px;">5521067726</p>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-4>\n      <img class="img-flamin" src="../../assets/imgs/flamingo.png">\n    </ion-col>\n    <ion-col class="col-social" col-8>\n      <p>SIGUENOS</p>\n      <img style="width: 15%;margin-right: 15%;" src="../../assets/imgs/facebook.png">\n      <img  style="width: 29%;" src="../../assets/imgs/instagram.png">\n      <p>TENEMOS ESTACIONAMIENTO</p>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/visit/visit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], VisitPage);
    return VisitPage;
}());

//# sourceMappingURL=visit.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThanksPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ThanksPage = /** @class */ (function () {
    function ThanksPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ThanksPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ThanksPage');
    };
    ThanksPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-thanks',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/thanks/thanks.html"*/'<!--\n  Generated template for the ThanksPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="light">\n    <ion-title>CHIC MUA</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<div class="test">\n  <img  style="width:62%;" src="../../assets/imgs/colibri.png">\n</div>\n<ion-content  class="thanks-page" padding>\n  <ion-row style="margin-top: 35%;">\n    <ion-col class="col-trans">\n      <p class="p-thanks">RECIBIRAS UN EMAIL CON LA CONFIRMACIÓN DE TU PEDIDO</p>\n      <p class="p-desc">TU PEDIDO SERÁ ENTREGADO DE 10 A 15 DÍAS</p>\n    </ion-col>\n  </ion-row>\n  <img  class="img-visit" src="../../assets/imgs/visitanos.png">\n  <ion-row>\n    <p class="p-finish">Gracias por tu compra!</p>\n  </ion-row>\n</ion-content>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/thanks/thanks.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], ThanksPage);
    return ThanksPage;
}());

//# sourceMappingURL=thanks.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(234);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_cloth_cloth__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_models_models__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_size_size__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_review_model_review_model__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_help_help__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_add_other_add_other__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_visit_visit__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_checkout_checkout__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_login_login__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_thanks_thanks__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_http__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__app_component__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_angular_svg_round_progressbar__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_angular_svg_round_progressbar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_20_angular_svg_round_progressbar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_common_http__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





/* Pages */













/*Plugins*/




var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_19__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */], __WEBPACK_IMPORTED_MODULE_7__pages_cloth_cloth__["a" /* ClothPage */], __WEBPACK_IMPORTED_MODULE_8__pages_models_models__["a" /* ModelsPage */], __WEBPACK_IMPORTED_MODULE_9__pages_size_size__["a" /* SizePage */], __WEBPACK_IMPORTED_MODULE_10__pages_review_model_review_model__["a" /* ReviewModelPage */], __WEBPACK_IMPORTED_MODULE_11__pages_help_help__["a" /* HelpPage */], __WEBPACK_IMPORTED_MODULE_12__pages_add_other_add_other__["a" /* AddOtherPage */], __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__["a" /* ProfilePage */], __WEBPACK_IMPORTED_MODULE_14__pages_visit_visit__["a" /* VisitPage */], __WEBPACK_IMPORTED_MODULE_15__pages_checkout_checkout__["a" /* CheckoutPage */], __WEBPACK_IMPORTED_MODULE_16__pages_login_login__["a" /* LoginPage */], __WEBPACK_IMPORTED_MODULE_17__pages_thanks_thanks__["a" /* ThanksPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_20_angular_svg_round_progressbar__["RoundProgressModule"],
                __WEBPACK_IMPORTED_MODULE_21__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_19__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_19__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */], __WEBPACK_IMPORTED_MODULE_7__pages_cloth_cloth__["a" /* ClothPage */], __WEBPACK_IMPORTED_MODULE_8__pages_models_models__["a" /* ModelsPage */], __WEBPACK_IMPORTED_MODULE_9__pages_size_size__["a" /* SizePage */], __WEBPACK_IMPORTED_MODULE_10__pages_review_model_review_model__["a" /* ReviewModelPage */], __WEBPACK_IMPORTED_MODULE_11__pages_help_help__["a" /* HelpPage */], __WEBPACK_IMPORTED_MODULE_12__pages_add_other_add_other__["a" /* AddOtherPage */], __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__["a" /* ProfilePage */], __WEBPACK_IMPORTED_MODULE_14__pages_visit_visit__["a" /* VisitPage */], __WEBPACK_IMPORTED_MODULE_15__pages_checkout_checkout__["a" /* CheckoutPage */], __WEBPACK_IMPORTED_MODULE_16__pages_login_login__["a" /* LoginPage */], __WEBPACK_IMPORTED_MODULE_17__pages_thanks_thanks__["a" /* ThanksPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_http__["a" /* HTTP */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CheckoutPage = /** @class */ (function () {
    function CheckoutPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CheckoutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CheckoutPage');
    };
    CheckoutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-checkout',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/checkout/checkout.html"*/'<!--\n  Generated template for the CheckoutPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>checkout</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/checkout/checkout.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], CheckoutPage);
    return CheckoutPage;
}());

//# sourceMappingURL=checkout.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--<ion-header>\n\n  <ion-navbar>\n    <ion-title>CHIC MUA</ion-title>\n  </ion-navbar>\n\n</ion-header>-->\n\n\n<ion-content [attr.noScroll]="shouldScroll" class="login-page" padding>\n<ion-grid>\n  <ion-row>\n    <ion-col style="text-align: center;">\n      <img class="img-logo" src="../../assets/imgs/chic-mua-logo.png">\n    </ion-col>\n  </ion-row>\n    <ion-row style="margin-top: -14%;">\n      <ion-col>\n        <ion-item class="item-peke">\n          <!--<ion-input [(ngModel)] = "token" style="display: none;"></ion-input>-->\n          <ion-label floating>Email</ion-label>\n          <ion-input type="email" [(ngModel)] = "email"></ion-input>\n        </ion-item>\n        <ion-item class="item-peke">\n          <ion-label floating>Contraseña</ion-label>\n          <ion-input type="password" [(ngModel)] = "pass"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row class="remember">\n      <ion-col col-12>\n        <ion-item class="item-checkbox-peke">\n          <ion-label style="font-size: 12px;">Recuerdame</ion-label>\n          <ion-checkbox [(ngModel)]="remember"></ion-checkbox>\n        </ion-item>\n      </ion-col>\n      <!--<ion-col col-12>\n        <ion-item>\n          <ion-label class="rememberme" style="" (click)="reset()">¿Olvidaste tu contraseña?</ion-label>\n        </ion-item>\n      </ion-col>-->\n    </ion-row>\n  <ion-row>\n    <ion-col style="text-align: center;">\n        <button class="button-save" ion-button  (click)="login()">Ingresar</button>\n        <button class="button-create" ion-button  (click)="login()">CREAR CUENTA</button>\n      <p>¿Olvidaste tu contraseña?</p>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/asus/Documentos/IonicProjects/chicmuaapp/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return URL_SERVICES; });
/* unused harmony export URL_LOGIN */
/* unused harmony export URL_GET_LOCATION */
/* unused harmony export KEY_MAPS */
//export const  URL_SERVICES = "http://18.191.211.112";
//export const  URL_LOGIN = "http://18.191.211.112";
//export const  URL_SERVICES = "http://puntopeke.com";
//export const  URL_SERVICES = "https://ingresar.puntopeke.com";
//export const  URL_SERVICES = "http://puntopeke.pruebasqubit.esy.es";
var URL_SERVICES = "http://chicmua.pruebasqubit.esy.es";
//export const  URL_LOGIN = "http://puntopeke.pruebasqubit.esy.es";
var URL_LOGIN = "http://chicmua.pruebasqubit.esy.es";
var URL_GET_LOCATION = "https://maps.googleapis.com/maps/api/geocode/json?address=";
var KEY_MAPS = "&key=AIzaSyBYzUPttIV8RX3zsIZRkkJACuC4w-ukE0Y";
//# sourceMappingURL=url.services.js.map

/***/ })

},[213]);
//# sourceMappingURL=main.js.map