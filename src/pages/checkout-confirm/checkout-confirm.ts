import { Component } from '@angular/core';
import { NavController, NavParams , LoadingController,AlertController} from 'ionic-angular';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {URL_SERVICES} from "../../config/url.services";
import {Storage} from "@ionic/storage";
import {AddAddressPage} from "../add-address/add-address";
import {CheckoutNextPage} from "../checkout-next/checkout-next";
import {AddCardPage} from "../add-card/add-card";
import {ThanksPage} from "../thanks/thanks";

declare var OpenPay: any;
@Component({
  selector: 'page-checkout-confirm',
  templateUrl: 'checkout-confirm.html',
})
export class CheckoutConfirmPage {
    private items:any;
    private cupon:any;
    private comentario:any;
    private friend:any;
    private send:any;
    private action:any;
    private all:any;
    private cards:any;
    private response:any;
    private addresses_data:any;
    private addresses:any;
    private address:any;
    private load:any;
    private subtotal:any;
    private coust:any;
    private total:any;
    private payment:any;
    private recal:any;
    private customerDiscount:any;
    private discount:any=0;
    public urlimg:any;
    public delivery:any;
    public confirm_data:any;
    public confirm:any;
    public cvv:any;
    public res:any;
    public deviceSessionId:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: HttpClient, public loading:LoadingController, private storage: Storage,private altrCtrl:AlertController) {
      OpenPay.setId('mglzss2usecdekpdud17');
      OpenPay.setApiKey('pk_cce5b5e14af24a21b7a841702e8040c1');
      OpenPay.setSandboxMode(false);
      this.deviceSessionId = OpenPay.deviceData.setup('customer-form', 'deviceIdHiddenFieldName');
      console.log('this.deviceSessionId ', this.deviceSessionId );
      this.load = this.loading.create({
          content: 'Cargando...'
      });
      this.load.present();
      this.payment=this.navParams.get("payment");
      this.delivery=this.navParams.get("delivery");
      this.cupon=this.navParams.get("cupon");
      this.address=this.navParams.get("address");
      this.action=this.navParams.get("action");
      this.urlimg = URL_SERVICES;
      this.getOrder();
      this.test();
      this.getCards();
      this.getCard();
      console.log(this.payment);
  }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CheckoutPage');
    }
    ionViewWillEnter(){
        this.getOrder();
        this.test();
        this.getCards();
        this.getCard();
    }

    getOrder(){
        this.storage.get('id_order').then((order)=>{
            console.log(order);
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":2,
                "orderid":order,
                "cliente":0,
                "shiping":0,
                "coupousoIdnDiscount":0,
                "size":0,
                "patern":0,
                "showpieze":0,
                "use":0,
                "address":0,
                "glasses":0,
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                console.log(response);
                this.all = response;
                this.items = this.all.data;
                this.subtotal=this.items[0].subtotal;
                this.total=this.items[0].total;
                this.customerDiscount=this.items[0].monto_descuento_cliente;
                this.coust=0;
                console.log(this.items[0].monto_descuento_cliente);
                this.load.dismiss().then(() => {
                    console.log('response success');
                });
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
        });
    }
    deleteAddress(id){
        this.storage.get('token').then((token) => {
            console.log(token);
            let url = URL_SERVICES + "/api/customer/delete_address";
            let headers = new HttpHeaders();
            let js = JSON.stringify({
                "address":id
            });
            //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers = headers.append('Content-Type', 'application/json');
            headers= headers.append('Authorization', 'Bearer '+ token);
            this.http.post(url,js,{headers:headers}).subscribe(response => {
                //console.log(response);
                this.response = response;
                if(this.response.ok == true){
                    this.altrCtrl.create({
                        title:"Exito",
                        subTitle:this.response.data,
                        buttons:["Entendido"]
                    }).present();
                }else{
                    this.altrCtrl.create({
                        title:"¿Teniendo Problemas?",
                        subTitle:'Para soporte marca: 55-3888-1413',
                        buttons:[
                            {
                                text: 'Soporte',
                                handler: () => {
                                    console.log('Soporte clicked');
                                    window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                }
                            },
                            {
                                text: 'Entendido',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    }
    sendAddAddress(){
        this.navCtrl.push(AddAddressPage);
    }
    next(){
        this.navCtrl.push(CheckoutNextPage);
    }
    test(){
        this.storage.get('id_order').then((order)=>{
            console.log(order);
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":this.action,
                "orderid":order,
                "cliente":0,
                "shiping":this.delivery,
                "coupousoIdnDiscount":this.cupon ? this.cupon:0,
                "size":0,
                "patern":0,
                "showpieze":0,
                "use":0,
                "address":this.address.id,
                "glasses":0,
                "linea":0,
                "friend":'',
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                console.log(response);
                this.recal = response;
                this.total = this.recal.data[0].Total;
                this.subtotal = this.recal.data[0].Subtotal;
                this.coust = this.recal.data[0].costo_envio;
                this.discount = this.recal.data[0].monto_descuento_cupon;
                this.customerDiscount = this.recal.data[0].monto_descuento_cliente;
                /* this.items = this.all.data;
                 this.subtotal=this.items[0].subtotal;
                 this.load.dismiss().then(() => {
                     console.log('response success');
                 });*/
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
        });
    }

    nextCheckout(address){
        if(this.delivery == 1){
            this.navCtrl.push(CheckoutConfirmPage,{delivery:this.delivery,cupon:this.cupon})
        }else{
            this.navCtrl.push(CheckoutNextPage,{address:address.id,delivery:this.delivery,cupon:this.cupon})
        }
    }
    sendAddPayment(){
        this.navCtrl.push(AddCardPage);
    }
    getCards(){
        this.storage.get('token').then((token) => {
            console.log(token);
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/payments/get_cards";
                let headers = new HttpHeaders();
                let js = JSON.stringify({
                    "customer":data
                });
                //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    this.cards = response;
                    console.log(response);
                    this.load.dismiss();
                });
            });
        });
    }
    payOpenPay(){
        this.altrCtrl.create({
            title:"Confirmar Pedido",
            subTitle:"¿Desea confirmar este pedido?",
            buttons:[
                {
                text:"Cancelar",
                role:"cancel",
            },
                {
                text:"Aceptar",
                role:"accept",
                    handler:()=>{
                        this.load = this.loading.create({
                            content:"Cargando ..."
                        });
                        this.load.present();
                        this.storage.get("token").then((token)=>{
                            this.storage.get('id').then((id)=>{
                                let url = URL_SERVICES + "/api/customer/payments/payment_customer";
                                let headers = new HttpHeaders();
                                headers = headers.append('Content-Type', 'application/json');
                                headers= headers.append('Authorization', 'Bearer '+token);
                                var js = {
                                    "customer":id,
                                    "cvv":this.cvv,
                                    "token_id":this.payment.token,
                                    "amount":this.total,
                                    "description":"pago de orden chic mua",
                                    "device_session_id":this.deviceSessionId
                                };
                                console.log(js);
                                this.http.post(url,js,{headers:headers}).subscribe(response => {
                                    console.log(response);
                                    this.res = response;
                                    this.load.dismiss().then(() => {
                                        console.log('response success');
                                    });
                                    if(this.res.status == true){
                                        this.altrCtrl.create({
                                            title:"Pago exitoso",
                                            subTitle:"Tu pago se realizo correctamente",
                                            buttons:["Entendido"]
                                        }).present();
                                        this.confirmOrder();
                                    }else{
                                        this.altrCtrl.create({
                                            title:"El pago no pudo ser realizado",
                                            subTitle:this.codes(this.res.code),
                                            buttons:["Entendido"]
                                        }).present();
                                    }
                                });
                            });
                        });
                    }
                }

            ]
        }).present();


        /**/
    }
    confirmOrder(){
      this.storage.get("id").then((id)=>{
          this.storage.get('id_order').then((order)=>{
              console.log(order);
              let url = URL_SERVICES + "/api/procedures/set_orders";
              let headers = new HttpHeaders();
              headers = headers.append('Content-Type', 'application/json');
              this.send = {
                  "accion":4,
                  "orderid":order,
                  "cliente":id,
                  "shiping":0,
                  "coupousoIdnDiscount":0,
                  "size":0,
                  "patern":0,
                  "showpieze":0,
                  "use":0,
                  "address":0,
                  "glasses":0,
                  "linea":0,
                  "friend":this.friend,
                  "comentarios":this.comentario
              };
              console.log(this.send);
              //headers= headers.append('Authorization', 'Bearer '+data);
              this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                  console.log(response);
                  this.confirm_data = response;
                  if(this.confirm_data.status == true){
                      this.storage.remove('id_order');
                      this.navCtrl.push(ThanksPage,{start:this.confirm_data.data[0].entrega_inicio,end:this.confirm_data.data[0].entrega_fin});
                  }else{

                  }
              });
          });
      });
    }
    getCard(){
        console.log(this.payment);
    }
    codes(code){
        var text;
        switch(code) {
            case 3001: {
                //statements;
                text="La tarjeta fue rechazada";
                break;
            }
            case 3002: {
                //statements;
                text="La tarjeta ha expirado";
                break;
            }
            case 3003: {
                //statements;
                text="La tarjeta no tiene fondos suficientes";
                break;
            }
            case 3004: {
                //statements;
                text="La tarjeta ha sido identificada como una tarjeta robada";
                break;
            }
            case 3005: {
                //statements;
                text="La tarjeta ha sido rechazada por el sistema antifraudes";
                break;
            }
            default: {
                //statements;
                text="Ocurrio algo insesperado";
                break;
            }
        }
        return text;
    }
}
