import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {URL_SERVICES} from "../../config/url.services";

@Component({
  selector: 'page-promo',
  templateUrl: 'promo.html',
})
export class PromoPage {
  public img_url:any;
  public promo:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.img_url = URL_SERVICES;
    this.promo = this.navParams.get('promo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromoPage');
  }

}
