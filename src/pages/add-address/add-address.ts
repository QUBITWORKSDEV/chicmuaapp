import { Component } from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormArray, FormGroup, Validators} from '@angular/forms';
import {URL_SERVICES} from "../../config/url.services";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Storage} from "@ionic/storage";
import {CheckoutNextPage} from "../checkout-next/checkout-next";
@Component({
  selector: 'page-add-address',
  templateUrl: 'add-address.html',
})
export class AddAddressPage {
    private form: FormGroup;
    private states:any=[];
    private all:any;
    private id:any;
    private address:any;
    private nickname:any;
    private street:any;
    private vicinity:any;
    private number:any;
    private  numberext:any;
    private  state:any;
    private  municipality:any;
    private  postal_code:any;
    private  cupon:any;
    private  flag:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder, public http: HttpClient, private storage: Storage, private altrCtrl:AlertController) {
      this.form = this.formBuilder.group({
          street: ['', Validators.required],
          nickname: ['', Validators.required],
          vicinity: ['',Validators.required],
          number: ['',Validators.required],
          numberext: ['',Validators.required],
          state: ['',Validators.required],
          municipality: ['',Validators.required],
          postal_code: ['',Validators.required],
      });
      this.id = this.navParams.get("id");
      console.log(this.id);
      if(this.id != undefined) {
          this.getAddress();
          this.flag = true;
      }
      console.log(this.flag);
      this.getStates();
  }

  ionViewDidLoad() {
      console.log(this.flag);
    console.log('ionViewDidLoad AddAddressPage');
  }
  getStates(){
      let url = URL_SERVICES + "/api/customer/states";
      let headers = new HttpHeaders();
      headers = headers.append('Content-Type', 'application/json');
      //headers= headers.append('Authorization', 'Bearer '+data);
      this.http.get(url,{headers:headers}).subscribe(response => {
          //this.restaurants = response;
          //this.items = response;
          console.log(response);
          this.states = response;
          /*this.load.dismiss().then(() => {
              console.log('response success');
          });*/
      });
  }

  sendAddress(){
      this.storage.get('token').then((token) => {
          this.storage.get('id').then((data) => {
              console.log(data);
              let url = URL_SERVICES + "/api/customer/new_address";
              let headers = new HttpHeaders();
              let js = JSON.stringify({
                  "customer":data,
                  "nickname":this.form.value.nickname,
                  "street":this.form.value.street,
                  "internal_number":this.form.value.number,
                  "external_number":this.form.value.numberext,
                  "suburb":this.form.value.vicinity,
                  "delegation":this.form.value.municipality,
                  "postal_code":this.form.value.postal_code,
                  "state":this.form.value.state
              });
              headers = headers.append('Content-Type', 'application/json');
              headers= headers.append('Authorization', 'Bearer '+ token);
              this.http.post(url,js,{headers:headers}).subscribe(response => {
                  this.all = response;
                  if(this.all.ok == true){
                      this.altrCtrl.create({
                          title:"Exito",
                          subTitle:"Se han agregado una nueva dirección",
                          buttons:["Entendido"]
                      }).present();
                      this.navCtrl.push(CheckoutNextPage,{address:this.all.data,cupon:this.cupon});
                      console.log(response);
                  }else{
                      this.altrCtrl.create({
                          title:"no se han agregado una nueva dirección",
                          subTitle:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                          buttons:[
                              {
                                  text: 'Soporte',
                                  handler: () => {
                                      console.log('Soporte clicked');
                                      window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                  }
                              },
                              {
                                  text: 'Entendido',
                                  role: 'cancel',
                                  handler: () => {
                                      console.log('Cancel clicked');
                                  }
                              }
                          ]
                      }).present();
                  }
                  console.log(response);
              });
          });
      });
  }
  getAddress(){
      this.storage.get('id').then((data) => {
          console.log(data);
          let url = URL_SERVICES + "/api/procedures/get_adress_custumer";
          let headers = new HttpHeaders();
          let js = JSON.stringify({
              "id":this.id,
              "cliente":data
          });
          //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
          headers = headers.append('Content-Type', 'application/json');
          this.http.post(url,js,{headers:headers}).subscribe(response => {
              //this.all = response;
              this.address = response;
              this.nickname = this.address.data.nickname;
              this.street = this.address.data.street;
              this.vicinity = this.address.data.suburb;
              this.state = this.address.data.state_id;
              this.municipality = this.address.data.delegation;
              this.postal_code = this.address.data.postal_code;
              this.numberext = this.address.data.external_number;
              this.number = this.address.data.internal_number;
              console.log(response);
          });
      });
    }

    edit(){
        this.storage.get('token').then((token) => {
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/edit_address";
                let headers = new HttpHeaders();
                let js = JSON.stringify({
                    "customer":data,
                    "address":this.id,
                    "nickname":this.form.value.nickname,
                    "street":this.form.value.street,
                    "internal_number":this.form.value.number,
                    "external_number":this.form.value.numberext,
                    "suburb":this.form.value.vicinity,
                    "delegation":this.form.value.municipality,
                    "postal_code":this.form.value.postal_code,
                    "state":this.form.value.state
                });
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    this.all = response;
                    if(this.all.ok == true){
                        this.altrCtrl.create({
                            title:"Exito",
                            subTitle:"Se han actualizado la dirección",
                            buttons:["Entendido"]
                        }).present();
                        this.navCtrl.pop();
                        //console.log(response);
                    }else{
                        this.altrCtrl.create({
                            title:"no se han podido actualizar la dirección",
                            subTitle:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                            buttons:[
                                {
                                    text: 'Soporte',
                                    handler: () => {
                                        console.log('Soporte clicked');
                                        window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                    }
                                },
                                {
                                    text: 'Entendido',
                                    role: 'cancel',
                                    handler: () => {
                                        console.log('Cancel clicked');
                                    }
                                }
                            ]
                        }).present();
                    }
                    console.log(response);
                });
            });
        });
    }
}
