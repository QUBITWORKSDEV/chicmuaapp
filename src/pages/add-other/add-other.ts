import { Component } from '@angular/core';
import {CardHeader, NavController, NavParams, AlertController, Alert, App, ToastController,LoadingController} from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";
import {Storage} from "@ionic/storage";
import {CheckoutPage} from "../checkout/checkout";
import {LoginPage} from "../login/login";
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {URL_SERVICES} from "../../config/url.services";
@Component({
  selector: 'page-add-other',
  templateUrl: 'add-other.html',
})
export class AddOtherPage {
  private js:any;
  private res:any;
  private size:any;
  private send:any;
  private backgrounds:any;
  private background:any;
  private load:any;
  private url_image:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,  private storage: Storage, public http: HttpClient, public altrCtrl:AlertController,private app:App,public toastCtrl:ToastController, public loading:LoadingController) {
      this.js = this.navParams.get("test");
      this.size = this.navParams.get("size");
      this.backgrounds = this.navParams.get("backgrounds");
      this.background = this.backgrounds[0].imagencart;
      this.url_image = URL_SERVICES;
      console.log(this.js);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddOtherPage');
    this.presentToast();
  }

  addOther(){
      this.storage.get('id').then((data) => {
          console.log(data);
          if(data != null){
              this.storage.get('id_order').then((data)=>{
                  console.log(data);
                  if(data!=null){
                    this.setOrder(1);
                  }else{
                      this.setNewOrder();
                  }
              });
              //this.navCtrl.setRoot(TabsPage);
          }else{
              this.navCtrl.push(LoginPage,{page:2,js:this.js});
          }
      });
  }

  checkout(){
          this.storage.get('id').then((data) => {
              console.log(data);
              if(data != null){
                  this.storage.get('id_order').then((data)=>{
                      console.log(data);
                      if(data!=null){
                          this.setOrder(2);
                      }else{
                          this.setNewOrder();
                      }
                  });
                  //this.navCtrl.push(CheckoutPage,{action:2});
              }else{
                  this.navCtrl.push(LoginPage,{page:2,js:this.js});
              }
          });
  }
    setOrder(action){
        this.load = this.loading.create({
            content: 'Cargando...'
        });
        this.storage.get('id').then((id)=> {
            this.storage.get('id_order').then((order)=>{
                let url = URL_SERVICES + "/api/procedures/set_orders";
                let headers = new HttpHeaders();
                headers = headers.append('Content-Type', 'application/json');
                this.send = {
                    "accion":1,
                    "orderid":order,
                    "cliente":id,
                    "shiping":0,
                    "coupousoIdnDiscount":0,
                    "size":this.js.size.id_talla,
                    "patern":this.js.pattern.id_tela,
                    "showpieze":this.js.model.id_modelo,
                    "use":this.js.uso,
                    "address":0,
                    "glasses":this.js.copas
                };
                console.log(this.send);
                //headers= headers.append('Authorization', 'Bearer '+data);
                this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                    //this.restaurants = response;
                    //this.items = response;
                    console.log(response);
                    this.res = response;
                    if(this.res.status == true){
                        this.load.dismiss().then(() => {
                            console.log('response success');
                        });
                        this.altrCtrl.create({
                            title:"Perfecto",
                            subTitle:"Se agrego una prenda mas a tu carrito",
                            buttons:["OK"]
                        }).present();
                        if(action == 1){
                            this.app.getRootNav().setRoot(TabsPage);
                        }else{
                            this.navCtrl.push(CheckoutPage,{action:2});
                        }
                    }else{
                        this.altrCtrl.create({
                            title:"Ocurrio algo inesperado!",
                            subTitle:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                            buttons:[
                                {
                                    text: 'Soporte',
                                    handler: () => {
                                        console.log('Soporte clicked');
                                        window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                    }
                                },
                                {
                                    text: 'Entendido',
                                    role: 'cancel',
                                    handler: () => {
                                        console.log('Cancel clicked');
                                    }
                                }
                            ]
                        }).present();
                    }
                });
            });
        });
    }

    setNewOrder(){
        this.storage.get('id').then((id)=> {
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":1,
                "orderid":0,
                "cliente":id,
                "shiping":0,
                "coupousoIdnDiscount":0,
                "size":this.js.size.id_talla,
                "patern":this.js.pattern.id_tela,
                "showpieze":this.js.model.id_modelo,
                "use":this.js.uso,
                "address":0,
                "glasses":this.js.copas
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                //this.items = response;
                console.log(response);
                this.res = response;
                if(this.res.status == true){
                    this.storage.set('id_order', this.res.data[0].id);
                    //alert(this.res.data[0].id);
                    //this.navCtrl.push(CheckoutPage,{page:2,action:2})
                    this.altrCtrl.create({
                        title:"Perfecto",
                        subTitle:"Se agrego una prenda mas a tu carrito",
                        buttons:["OK"]
                    }).present();
                    this.app.getRootNav().setRoot(TabsPage);
                }else{
                    this.altrCtrl.create({
                        title:"Ocurrio algo inesperado!",
                        subTitle:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                        buttons:[
                            {
                                text: 'Soporte',
                                handler: () => {
                                    console.log('Soporte clicked');
                                    window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                }
                            },
                            {
                                text: 'Entendido',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    }
    presentToast() {
        const toast = this.toastCtrl.create({
            message: 'Elegiste '+this.size,
            duration: 2000,
            position:'middle',
            cssClass:'toast',
            dismissOnPageChange: true
        });
        toast.present();
    }

}
