import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SocialSharing} from "@ionic-native/social-sharing";
import {TabsPage} from "../tabs/tabs";
@Component({
  selector: 'page-share',
  templateUrl: 'share.html',
})
export class SharePage {
  private open:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private socialSharing: SocialSharing) {
      this.open = this.navParams.get("open");
     this.sharing();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SharePage');
  }

    public share(){
        this.navCtrl.setRoot(TabsPage,{share:true});
    }
    public sharing(){
        let msg ="Te invito a ser parte de nosotros ChicMua en android : https://play.google.com/store/apps/details?id=com.qubitworks.chicmua y en iOS: https://itunes.apple.com/us/app/chic-mua/id1455868804";
          let subject = "Compartir Chicmua";
          let url = "";

          this.socialSharing.share(msg,subject, null , url)
              .then(() => {
                  //this.presentToast( "ok!" );
                  //this.sendLog();
                  //this.navCtrl.pop();
              })
              .catch((err) => {
                  //this.presentToast( err );
              });
    }
}
