import { Component } from '@angular/core';
import {AlertController, App, LoadingController, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormArray, FormGroup, Validators} from '@angular/forms';
import {AddAddressPage} from "../add-address/add-address";
import {AddCardPage} from "../add-card/add-card";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {URL_SERVICES} from "../../config/url.services";
import {Storage} from "@ionic/storage";
import {TabsPage} from "../tabs/tabs";
import {TermsPage} from "../terms/terms";
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
    private form: FormGroup;
    private user:any;
    private name:any;
    private all:any;
    private cards:any=[];
    private firstname:any;
    private secondname:any;
    private birthdate:any;
    private genre:any;
    private email:any;
    private phone:any;
    private addresses:any;
    private addresses_data:any;
    private response:any;
    private load:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder, private storage: Storage, public http: HttpClient,  private app:App, private altrCtrl:AlertController, public loading:LoadingController) {
      this.form = this.formBuilder.group({
          name: ['', Validators.required],
          firstname: ['',Validators.required],
          secondname: ['',Validators.required],
          birthdate: ['',Validators.required],
          genre: ['',Validators.required],
          email: ['',Validators.email],
          phone: ['',Validators.required],
          lada: ['',]
      });
      this.load = this.loading.create({
          content: 'Cargando...'
      });
      this.load.present();
      this.getUser();
      this.getCards();
      this.getAddresses();
      //this.getCard();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
    ionViewWillEnter(){
        this.getUser();
        this.getCards();
        this.getAddresses();
        //this.getCard();
    }

  sendAddAddress(){
      this.navCtrl.push(AddAddressPage);
  }

  sendAddPayment(){
      this.navCtrl.push(AddCardPage);
  }
    getUser(){
        this.storage.get('token').then((token) => {
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/profile";
                let headers = new HttpHeaders();
                let js = JSON.stringify({
                    "customer":data
                });
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    console.log(response);
                    this.user = response;
                    this.name  = this.user.name;
                    this.firstname  = this.user.first_name;
                    this.secondname  = this.user.second_name;
                    this.genre   = this.user.gender;
                    this.email = this.user.email;
                    this.phone = this.user.phone;
                    this.birthdate = new Date(this.user.birth_date).toISOString();
                });
            });
        });
    }
    logForm(){
        this.storage.get('id').then((data) => {
            console.log(data);
            let url = URL_SERVICES + "/api/procedures/set_perfil";
            let headers = new HttpHeaders();
            let js = JSON.stringify({
                "cliente":data,
                "nombre":this.form.value.name,
                "apepat":this.form.value.firstname,
                "apemat":this.form.value.secondname,
                "email":this.form.value.email,
                "genero":this.form.value.genre,
                "fechnac":this.form.value.birthdate,
                "phone":this.form.value.phone
            });
            console.log(js);
            headers = headers.append('Content-Type', 'application/json');
           // headers= headers.append('Authorization', 'Bearer '+ token);
            this.http.post(url,js,{headers:headers}).subscribe(response => {
                this.response = response;
                if(this.response.status == true){
                    this.altrCtrl.create({
                        title:"Exito",
                        subTitle:"Se han actualizado tus datos de manera correcta",
                        buttons:["Entendido"]
                    }).present();
                    this.app.getRootNav().setRoot(TabsPage);
                }else{

                }
                console.log(response);
            });
        });
    }
    getCards(){
        this.storage.get('token').then((token) => {
            console.log(token);
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/payments/get_cards";
                let headers = new HttpHeaders();
                let js = JSON.stringify({
                    "customer":data
                });
                //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    this.all = response;
                    this.cards = this.all.data;
                    console.log(response);
                    this.load.dismiss();
                });
            });
        });
    }
    /*getCard(){
        this.storage.get('token').then((token) => {
            console.log(token);
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/payments/get_card";
                let headers = new HttpHeaders();
                let js = JSON.stringify({
                    "customer": data,
                    "card": 2,
                    "token":"kjcqmm8mw6whkiop4evi"
                });
                //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    console.log(response);
                });
            });
        });
    }*/
    getAddresses(){
        this.storage.get('id').then((data) => {
            console.log(data);
            let url = URL_SERVICES + "/api/procedures/get_adresses_custumer";
            let headers = new HttpHeaders();
            let js = JSON.stringify({
                "cliente":data
            });
            //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers = headers.append('Content-Type', 'application/json');
            //headers= headers.append('Authorization', 'Bearer '+ token);
            this.http.post(url,js,{headers:headers}).subscribe(response => {
                this.addresses_data = response;
                this.addresses = this.addresses_data.data;
                console.log(response);
            });
        });
    }

    addressSelect(address){
        this.navCtrl.push(AddAddressPage, {id:address.id})
    }
    deleteAddress(id){
        this.storage.get('token').then((token) => {
            console.log(token);
                let url = URL_SERVICES + "/api/customer/delete_address";
                let headers = new HttpHeaders();
            let js = JSON.stringify({
                "address":id
            });
                //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    console.log(response);
                    this.response = response;
                    if(this.response.ok == true){
                        this.altrCtrl.create({
                            title:"Exito",
                            subTitle:this.response.data,
                            buttons:["Entendido"]
                        }).present();
                    }else{
                        this.altrCtrl.create({
                            title:this.response.data,
                            subTitle:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                            buttons:[
                                {
                                    text: 'Soporte',
                                    handler: () => {
                                        console.log('Soporte clicked');
                                        window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                    }
                                },
                                {
                                    text: 'Entendido',
                                    role: 'cancel',
                                    handler: () => {
                                        console.log('Cancel clicked');
                                    }
                                }
                            ]
                        }).present();
                    }
                });
        });
    }
    deleteCard(id){
            this.storage.get('token').then((token) => {
                console.log(token);
                let url = URL_SERVICES + "/api/customer/payments/delete_card";
                let headers = new HttpHeaders();
                let js = JSON.stringify({
                    "id":id
                });
                //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    console.log(response);
                    this.response = response;
                    if(this.response.ok == true){
                        this.altrCtrl.create({
                            title:"Exito",
                            subTitle:this.response.data,
                            buttons:[
                                {
                                    text: 'Entendido',
                                    role: 'cancel',
                                    handler: () => {
                                        this.getCards();
                                    }
                                }
                            ]
                        }).present();
                    }else{
                        this.altrCtrl.create({
                            title:this.response.data,
                            subTitle:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                            buttons:[
                                {
                                    text: 'Soporte',
                                    handler: () => {
                                        console.log('Soporte clicked');
                                        window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                    }
                                },
                                {
                                    text: 'Entendido',
                                    role: 'cancel',
                                    handler: () => {
                                        console.log('Cancel clicked');
                                    }
                                }
                            ]
                        }).present();
                    }
                });
            });
    }
    terms(){
        this.navCtrl.push(TermsPage);
    }
}
