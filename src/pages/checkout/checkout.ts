import { Component } from '@angular/core';
import { NavController, NavParams , LoadingController,AlertController} from 'ionic-angular';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {URL_SERVICES} from "../../config/url.services";
import {Storage} from "@ionic/storage";
import {AddAddressPage} from "../add-address/add-address";
import {CheckoutNextPage} from "../checkout-next/checkout-next";
import {CheckoutConfirmPage} from "../checkout-confirm/checkout-confirm";
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  private items:any;
  private cupon:any;
  private send:any;
  private action:any;
  private all:any;
  private response:any;
  private addresses_data:any;
  private addresses:any;
  private load:any;
  private subtotal:any;
  private coust:any;
  private total:any;
  private recal:any;
  private addressescount:any = 0;
  private customerDiscount:any;
  private discount:any=0;
    public urlimg:any;
  public delivery:any = 3;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: HttpClient, public loading:LoadingController, private storage: Storage,private altrCtrl:AlertController) {
      this.load = this.loading.create({
          content: 'Cargando...'
      });
      this.load.present();
    this.action=this.navParams.get("action");
    this.urlimg = URL_SERVICES;
    this.getOrder();
    this.getAddresses();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
  }
    ionViewWillEnter(){
        this.getOrder();
        this.getAddresses();
    }

    getOrder(){
        this.storage.get('id_order').then((order)=>{
            console.log(order);
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":this.action,
                "orderid":order,
                "cliente":0,
                "shiping":0,
                "coupousoIdnDiscount":0,
                "size":0,
                "patern":0,
                "showpieze":0,
                "use":0,
                "address":0,
                "glasses":0,
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                console.log(response);
                this.all = response;
                this.items = this.all.data;
                this.subtotal=this.items[0].subtotal;
                this.total=this.items[0].total;
                this.customerDiscount=this.items[0].monto_descuento_cliente;
                this.coust=0;
                console.log(this.items[0].monto_descuento_cliente);
                this.load.dismiss().then(() => {
                    console.log('response success');
                });
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
        });
    }
    getAddresses(){
        this.storage.get('id').then((data) => {
            console.log(data);
            let url = URL_SERVICES + "/api/procedures/get_adresses_custumer";
            let headers = new HttpHeaders();
            let js = JSON.stringify({
                "cliente":data
            });
            //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers = headers.append('Content-Type', 'application/json');
            //headers= headers.append('Authorization', 'Bearer '+ token);
            this.http.post(url,js,{headers:headers}).subscribe(response => {
                this.addresses_data = response;
                this.addresses = this.addresses_data.data;
                this.addressescount = this.addresses.length;
                console.log(response);
                console.log(this.addressescount);
            });
        });
    }

    addressSelect(address){
        this.navCtrl.push(AddAddressPage, {id:address.id})
    }
    deleteAddress(id){
        this.storage.get('token').then((token) => {
            console.log(token);
            let url = URL_SERVICES + "/api/customer/delete_address";
            let headers = new HttpHeaders();
            let js = JSON.stringify({
                "address":id
            });
            //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers = headers.append('Content-Type', 'application/json');
            headers= headers.append('Authorization', 'Bearer '+ token);
            this.http.post(url,js,{headers:headers}).subscribe(response => {
                //console.log(response);
                this.response = response;
                if(this.response.ok == true){
                    this.altrCtrl.create({
                        title:"Exito",
                        subTitle:this.response.data,
                        buttons:[
                            {
                                text: 'Entendido',
                                role: 'cancel',
                                handler: () => {
                                    this.getAddresses();
                                }
                            }
                        ]
                    }).present();
                }else{
                    this.altrCtrl.create({
                        title:this.response.data,
                        subTitle:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                        buttons:[
                            {
                                text: 'Entendido',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            },
                            {
                                text: 'Soporte',
                                handler: () => {
                                    console.log('Soporte clicked');
                                    window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    }
    sendAddAddress(){
        this.navCtrl.push(AddAddressPage,{cupon:this.cupon});
    }
    next(){
        this.navCtrl.push(CheckoutNextPage);
    }
    test(){
        this.storage.get('id_order').then((order)=>{
            console.log(order);
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":3,
                "orderid":order,
                "cliente":0,
                "shiping":1,
                "coupousoIdnDiscount":this.cupon ? this.cupon:0,
                "size":0,
                "patern":0,
                "showpieze":0,
                "use":0,
                "address":0,
                "glasses":0,
                "linea":0,
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                console.log(response);
                this.recal = response;
                this.total = this.recal.data[0].Total;
                this.subtotal = this.recal.data[0].Subtotal;
                this.coust = this.recal.data[0].costo_envio;
                this.discount = this.recal.data[0].monto_descuento_cupon;
                this.customerDiscount = this.recal.data[0].monto_descuento_cliente;
               /* this.items = this.all.data;
                this.subtotal=this.items[0].subtotal;
                this.load.dismiss().then(() => {
                    console.log('response success');
                });*/
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
        });
    }

    nextCheckout(address?,push?){
        console.log(push);
        if(this.delivery == 3 && push == 0){
            this.altrCtrl.create({
                title:"Selecciona",
                subTitle:"Tienes que seleccionar una direccion de envio",
                buttons:["Entendido"]
            }).present();
        }
        if(this.delivery == 1 || push == 1){
            this.delivery = 1;
            this.navCtrl.push(CheckoutNextPage,{address:0,delivery:this.delivery,cupon:this.cupon,action:3})
        }
        if(this.delivery == 2 || push == 2){
          //  this.navCtrl.push(CheckoutNextPage,{address:address,delivery:this.delivery,cupon:this.cupon, action:3})
            this.navCtrl.push(CheckoutNextPage,{address:address,delivery:2,cupon:this.cupon, action:3})
        }
    }
    store(){
        this.altrCtrl.create({
            title:"Dirección de la tienda",
            subTitle:"Durango 25 Col.Progreso Tizapán, México, CDMX",
            buttons:["Entendido"]
        }).present();
    }
    deline(line){
        this.altrCtrl.create({
            title:"Eliminar Prenda",
            subTitle:'¿Seguro que desea eliminar prenda ?',
            buttons:[
                {
                    text:'Cancelar',
                    role: 'cancel',
                    handler: () => {
                        
                    }
                },
                {
                    text:'Aceptar',
                    role: 'acept',
                    handler: () => {
                        this.storage.get('id_order').then((order)=>{
                            this.storage.get('id').then((id)=>{
                                console.log(order);
                                let url = URL_SERVICES + "/api/procedures/set_orders";
                                let headers = new HttpHeaders();
                                headers = headers.append('Content-Type', 'application/json');
                                this.send = {
                                    "accion":5,
                                    "orderid":order,
                                    "cliente":id,
                                    "shiping":1,
                                    "coupousoIdnDiscount":this.cupon ? this.cupon:0,
                                    "size":0,
                                    "patern":0,
                                    "showpieze":0,
                                    "use":0,
                                    "address":0,
                                    "glasses":0,
                                    "linea":line,
                                    "amigo":'0'
                                };
                                console.log(this.send);
                                //headers= headers.append('Authorization', 'Bearer '+data);
                                this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                                    //this.restaurants = response;
                                    console.log(response);
                                    this.all = response;
                                    if(this.all.status == true){
                                        this.getOrder();
                                    }else{
                                        this.altrCtrl.create({
                                            title:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                                            subTitle:'no se pudo eliminar la prenda',
                                            buttons:[
                                                {
                                                    text: 'Entendido',
                                                    role: 'cancel',
                                                    handler: () => {
                                                        console.log('Cancel clicked');
                                                    }
                                                },
                                                {
                                                    text: 'Soporte',
                                                    handler: () => {
                                                        console.log('Soporte clicked');
                                                        window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                                    }
                                                }
                                            ]
                                        }).present();
                                    }
                                });
                            });
                        });
                    }
                }
            ]
        }).present();
    }
}
