import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import {HomePage} from "../home/home";
import {ProfilePage} from "../profile/profile";
import {VisitPage} from "../visit/visit";
import {SharePage} from "../share/share";
import {TermsPage} from "../terms/terms";
import { Storage } from "@ionic/storage";
import { SocialSharing} from "@ionic-native/social-sharing";
import {LoginPage} from "../login/login";
import {URL_SERVICES} from "../../config/url.services";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CheckoutPage} from "../checkout/checkout";
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
    private page:any;
    private line:any;
    private send:any;
    private res:any;
    private all:any;
    private user:any;
    private share:any;
    tab1 = HomePage;
    tab2 :any;
    tab3 = SharePage;
    tab4 = VisitPage;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,public http: HttpClient, public altrCtrl:AlertController,private socialSharing: SocialSharing) {
      this.storage.get('loginState').then((value)=>{
          console.log(value);
          if(value == true) {
              //Comprobar si es la primera vez que abre la app
              this.tab2 = ProfilePage;
          }else{
              this.tab2= LoginPage;
          }
      }).catch(()=>{

      });
      this.share = this.navParams.get("share");
      console.log(this.share);
      this.page = this.navParams.get("page");
      this.line = this.navParams.get("line");
      console.log(this.page, this.line);
      if(this.page != undefined){
          this.sendOrder();
      }else{

      }
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad TabsPage');
      this.getUser();
      this.getUserOpenpay();
  }
    sendOrder(){
        this.storage.get('id').then((id)=> {
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":1,
                "orderid":0,
                "cliente":id,
                "shiping":0,
                "coupousoIdnDiscount":0,
                "size":this.line.size.id_talla,
                "patern":this.line.pattern.id_tela,
                "showpieze":this.line.model.id_modelo,
                "use":this.line.uso,
                "address":0,
                "glasses":this.line.copas
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                //this.items = response;
                console.log(response);
                this.res = response;
                if(this.res.status == true){
                    this.storage.set('id_order', this.res.data[0].id);
                    //alert(this.res.data[0].id);
                    //this.navCtrl.push(CheckoutPage,{page:2,action:2})
                }else{
                    this.altrCtrl.create({
                        title:"Ocurrio algo inesperado!",
                        subTitle:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                        buttons:[
                            {
                                text: 'Soporte',
                                handler: () => {
                                    console.log('Soporte clicked');
                                    window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                }
                            },
                            {
                                text: 'Entendido',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    }
    public sharing(){
        let msg ="Te invito a ser parte de nosotros ChicMua en android : https://play.google.com/store/apps/details?id=com.qubitworks.chicmua y en iOS: https://itunes.apple.com/us/app/chic-mua/id1455868804?ls=1&mt=8";
        let subject = "Compartir Chicmua";
        let url = "";

        this.socialSharing.share(msg,subject, null , url)
            .then(() => {
                //this.presentToast( "ok!" );
                //this.sendLog();
                //this.navCtrl.pop();
            })
            .catch((err) => {
                //this.presentToast( err );
            });
    }
    getUser(){
        this.storage.get('token').then((token) => {
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/profile";
                let headers = new HttpHeaders();
                let js = JSON.stringify({
                    "customer":data
                });
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    console.log(response);
                    this.user = response;
                });
            });
        });
    }
    getUserOpenpay() {
        this.storage.get('token').then((token) => {
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/payments/get_user";
                let headers = new HttpHeaders();
                headers = headers.append('Content-Type', 'application/json');
                headers = headers.append('Authorization', 'Bearer ' + token);
                let js = JSON.stringify({
                    "customer": data
                });
                this.http.post(url, js, {headers: headers}).subscribe(response => {
                    console.log(response);
                    this.all = response;
                    if(this.all.status == false || this.all.ok == false){
                        this.registerCustomerOpenPay();
                    }else{

                    }
                });
            });
        });
    }
    registerCustomerOpenPay() {
        console.log(this.user);
        this.storage.get('token').then((token) => {
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/payments/register_user";
                let headers = new HttpHeaders();
                headers = headers.append('Content-Type', 'application/json');
                headers = headers.append('Authorization', 'Bearer ' + token);
                let js = JSON.stringify({
                    "name":this.user.name,
                    "last_name":this.user.first_name,
                    "email":this.user.email,
                    "phone_number":this.user.phone,
                    "line1":"Durango 25 Col.Progreso Tizapan",
                    "line2":"Durango 25 Col.Progreso Tizapan",
                    "line3":"Durango 25 Col.Progreso Tizapan",
                    "postal_code":"53489",
                    "state":"Mexico",
                    "city":"Mexico",
                    "country_code":"MX",
                    "customer": data
                });
                console.log(js);
                this.http.post(url, js, {headers: headers}).subscribe(response => {
                    console.log(response);
                    //this.addCardOpen();
                });
            });
        });
    }
}
