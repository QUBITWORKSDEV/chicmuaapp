import { Component } from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {URL_SERVICES} from "../../config/url.services";
@Component({
  selector: 'page-tips',
  templateUrl: 'tips.html',
})
export class TipsPage {
  private load:any;
  private all:any;
  private urlimg:any;
  private uso:any;
  private tips:any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient,public loading:LoadingController) {
      this.load = this.loading.create({
          content: 'Cargando...'
      });
      this.uso = this.navParams.get("uso");
      this.load.present();
      this.urlimg = URL_SERVICES;
      this.getTips();
  }
  getTips(){
      let url = URL_SERVICES + "/api/procedures/get_consejos";
      let headers = new HttpHeaders();
      headers = headers.append('Content-Type', 'application/json');
      //headers= headers.append('Authorization', 'Bearer '+data);
      var js = {
          default:1,
          uso: this.uso
      };
      this.http.post(url,js,{headers:headers}).subscribe(response => {
          //this.restaurants = response;
          //this.items = response;
          this.all = response;
          this.tips = this.all.data;
          this.load.dismiss().then(() => {
              console.log('response success');
          });
          console.log(response);
      });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TipsPage');
  }

}
