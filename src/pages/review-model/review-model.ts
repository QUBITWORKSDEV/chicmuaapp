import { Component } from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {SizePage} from "../size/size";
import {URL_SERVICES} from "../../config/url.services";

@Component({
  selector: 'page-review-model',
  templateUrl: 'review-model.html',
})
export class ReviewModelPage {
    private js:any;
    private send:any;
    private show:any=0;
    public urlimg:any;
    public pattern:any;
    public backgrounds:any;
    public screen:any=1;
  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController, public toastCtrl:ToastController) {
      this.js = this.navParams.get("data");
      this.pattern = this.navParams.get("tela");
      this.backgrounds= this.navParams.get("backgrounds");
      this.urlimg = URL_SERVICES;
      console.log(this.js);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewModelPage');
    this.presentToast();
  }
  sendNext(send){
      console.log(send);
      if( send.copas == 1){
          let alert = this.alertCtrl.create({
              title:'¿Desea copas?',
              message: '$'+send.preciocopas+'pesos',
              buttons: [
                  {
                      text: 'Si',
                      role: 'cancel',
                      handler: () => {
                          //console.log('Cancel clicked');
                          this.send = {
                              uso:this.js.uso,
                              pattern:this.js.pattern,
                              model:this.js.model,
                              copas:1
                          };
                          this.navCtrl.push(SizePage,{data:this.send,backgrounds:this.backgrounds});
                      }
                  },
                  {
                      text: 'No',
                      handler: () => {
                          //console.log('Buy clicked');
                          this.send = {
                              uso:this.js.uso,
                              pattern:this.js.pattern,
                              model:this.js.model,
                              copas:0
                          };
                          this.navCtrl.push(SizePage, {data:this.send,backgrounds:this.backgrounds});
                      }
                  }
              ]
          });
          alert.present();
      }else{
          this.send = {
              uso:this.js.uso,
              pattern:this.js.pattern,
              model:this.js.model,
              copas:0
          };
          this.navCtrl.push(SizePage, {data:this.send,backgrounds:this.backgrounds});
      }
  }
    presentToast() {
        const toast = this.toastCtrl.create({
            message: 'Elegiste '+this.pattern,
            duration: 2000,
            position:'middle',
            cssClass:'toast',
            dismissOnPageChange: true
        });
        toast.present();
    }
    next(){
      this.show = 0;
      this.screen = 2;
    }
    back(){
        this.show = 0;
        this.screen = 1;
    }
    change(val){
        this.show = val;
    }
}
