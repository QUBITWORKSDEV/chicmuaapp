import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import {ClothPage} from "../cloth/cloth";
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {URL_SERVICES} from "../../config/url.services";
import {ModelsPage} from "../models/models";
import {Storage} from "@ionic/storage";
import {CheckoutPage} from "../checkout/checkout";
import {PromoPage} from "../promo/promo";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private items:any=[];
  private current:any;
  private max:any;
  private timer = 0;
  private timerSeg = 0;
  private timerMin = 0;
  private load:any;
  private res:any;
  private send:any;
  private check:any;
  private err:any;
  public img_data:any;
  public respuesta:any;
  public url_image:any;
  public imageback:any;
  public imageman:any;
  public imagewoman:any;
  public imagegirl:any;
  public imageboy:any;
  public priceadult:any;
  public pricenino:any;
  public labeltext:string;
  constructor(public navCtrl: NavController, public http: HttpClient, public loading:LoadingController,private storage: Storage) {
  //  this.initialItems();
    this.current=5;
    this.max=6000;
    this.url_image = URL_SERVICES;
    //this.startTimer();
    this.getTypes();
    this.load = this.loading.create({
       content:"Cargando ..."
    });
    this.load.present();
  }
    ionViewDidLoad() {
        this.getCheckout();
        this.getPromos();
    }
    ionViewWillEnter(){
       this.getCheckout();
       this.getPromos();
    }
  startTimer(){
      var intervalVar = setInterval(function () {
          //alert(this.timer++)
          if(this.timer == this.max){
              alert('se cumplio el tiempoooooo!!');
              clearInterval(intervalVar);
          }else{
              if(this.timerSeg == 59){
                  this.timerMin++;
                  this.timerSeg = 0;
              }else{
                  this.timer++;
                  this.timerSeg++;
              }
          }
      }.bind(this),1000)
  }
  sendCloth(item){
      this.navCtrl.push(ModelsPage,{use:item.id,use_name:item.name,backgrounds:this.respuesta,text:this.labeltext});
  }
  getTypes(){
        let url = URL_SERVICES + "/api/customer/uses";
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        //headers= headers.append('Authorization', 'Bearer '+data);
        this.http.get(url,{headers:headers}).subscribe(response => {
            //this.restaurants = response;
            this.items = response;
            console.log(response);
            this.load.dismiss().then(() => {
                console.log('response Success');
            });
        },
            err=>{
                console.log(err);
            }
            );
  }
    getCheckout(){
        this.storage.get('id_order').then((order)=>{
            console.log(order);
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":2,
                "orderid":order,
                "cliente":0,
                "shiping":0,
                "coupousoIdnDiscount":0,
                "size":0,
                "patern":0,
                "showpieze":0,
                "use":0,
                "address":0,
                "glasses":0,
                "linea":0,
                "friend":0
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                //this.items = response;
                this.res = response;
                this.check = this.res.data.length;
                console.log(this.check);
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            },
                err=>{
                this.err = err;
                    console.log(err);
                });
        });
    }
    checkout(){
        this.navCtrl.push(CheckoutPage,{action:2});
    }
    getPromos(){
        let url = URL_SERVICES + "/api/procedures/get_promos";
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        var js = {
            default:0
        };
        //headers= headers.append('Authorization', 'Bearer '+data);
        this.http.post(url,js,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                //this.items = response;
            this.img_data = response;
            this.respuesta = this.img_data.data;
                console.log(this.respuesta);
            this.imageback = this.respuesta[0].imagenback;
            this.imageman = this.respuesta[0].imagenman;
            this.imagewoman = this.respuesta[0].imagenwoman;
            this.imagegirl = this.respuesta[0].imagengirl;
            this.imageboy = this.respuesta[0].imagenboy;
            this.priceadult = this.respuesta[0].precioadulto;
            this.pricenino = this.respuesta[0].precionino;
            this.labeltext = this.respuesta[0].labeltext;
            console.log(this.imageback);
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
    }
    sendPromo(item){
        this.navCtrl.push(PromoPage,{promo:item});
    }
}
