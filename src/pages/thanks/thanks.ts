import { Component } from '@angular/core';
import {App, NavController, NavParams} from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";

@Component({
  selector: 'page-thanks',
  templateUrl: 'thanks.html',
})
export class ThanksPage {
  private start:any;
  private end:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private app:App) {
    this.start = this.navParams.get("start");
    this.end = this.navParams.get("end");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ThanksPage');
  }
  confirm(){
     this.app.getRootNav().setRoot(TabsPage);
  }

}
