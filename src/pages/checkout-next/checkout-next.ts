import { Component } from '@angular/core';
import { NavController, NavParams , LoadingController,AlertController} from 'ionic-angular';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {URL_SERVICES} from "../../config/url.services";
import {Storage} from "@ionic/storage";
import {AddAddressPage} from "../add-address/add-address";
import {CheckoutConfirmPage} from "../checkout-confirm/checkout-confirm";
import {AddCardPage} from "../add-card/add-card";
@Component({
  selector: 'page-checkout-next',
  templateUrl: 'checkout-next.html',
})
export class CheckoutNextPage {
    private items:any;
    private cupon:any;
    private send:any;
    private action:any;
    private all:any;
    private cards:any;
    private response:any;
    private addresses_data:any;
    private addresses:any;
    private address:any;
    private load:any;
    private subtotal:any;
    private coust:any;
    private total:any;
    private recal:any;
    private res:any;
    private customerDiscount:any;
    private discount:any=0;
    public urlimg:any;
    public delivery:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: HttpClient, public loading:LoadingController, private storage: Storage,private altrCtrl:AlertController) {
      this.load = this.loading.create({
          content: 'Cargando...'
      });
      this.load.present();
      this.delivery=this.navParams.get("delivery");
      this.cupon=this.navParams.get("cupon");
      this.address=this.navParams.get("address");
      this.action=this.navParams.get("action");
      this.urlimg = URL_SERVICES;
      this.getOrder();
      this.test();
      this.getCards();
      console.log(this.address);
  }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CheckoutPage');
    }
    ionViewWillEnter(){
        //this.getOrder();
        //this.test();
        this.getCards();
    }

    getOrder(){
        this.storage.get('id_order').then((order)=>{
            console.log(order);
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":2,
                "orderid":order,
                "cliente":0,
                "shiping":0,
                "coupousoIdnDiscount":0,
                "size":0,
                "patern":0,
                "showpieze":0,
                "use":0,
                "address":0,
                "glasses":0,
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                console.log(response);
                this.all = response;
                this.items = this.all.data;
                this.subtotal=this.items[0].subtotal;
                this.total=this.items[0].total;
                this.customerDiscount=this.items[0].monto_descuento_cliente;
                this.coust=this.items[0].costo_ensrcvio;
                console.log(this.items[0].monto_descuento_cliente);
                this.load.dismiss().then(() => {
                    console.log('response success');
                });
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
        });
    }
      deleteAddress(id){
        this.storage.get('token').then((token) => {
            console.log(token);
            let url = URL_SERVICES + "/api/customer/delete_address";
            let headers = new HttpHeaders();
            let js = JSON.stringify({
                "address":id
            });
            //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers = headers.append('Content-Type', 'application/json');
            headers= headers.append('Authorization', 'Bearer '+ token);
            this.http.post(url,js,{headers:headers}).subscribe(response => {
                //console.log(response);
                this.response = response;
                if(this.response.ok == true){
                    this.altrCtrl.create({
                        title:"Exito",
                        subTitle:this.response.data,
                        buttons:["Entendido"]
                    }).present();
                }else{
                    this.altrCtrl.create({
                        title:"¿Teniendo Problemas?",
                        subTitle:"Para soporte marca: 55-3888-1413",
                        buttons:[
                            {
                                text: 'Soporte',
                                handler: () => {
                                    console.log('Soporte clicked');
                                    window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                }
                            },
                            {
                                text: 'Entendido',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    }
    sendAddAddress(){
        this.navCtrl.push(AddAddressPage);
    }
    next(){
        this.navCtrl.push(CheckoutNextPage);
    }
    test(){
        this.storage.get('id_order').then((order)=>{
            console.log(order);
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":this.action,
                "orderid":order,
                "cliente":0,
                "shiping":this.delivery,
                "coupousoIdnDiscount":this.cupon ? this.cupon:0,
                "size":0,
                "patern":0,
                "showpieze":0,
                "use":0,
                "address":this.address.id,
                "glasses":0,
                "linea":0,
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                console.log(response);
                this.recal = response;
                this.total = this.recal.data[0].Total;
                this.subtotal = this.recal.data[0].Subtotal;
                this.coust = this.recal.data[0].costo_envio;
                this.discount = this.recal.data[0].monto_descuento_cupon;
                this.customerDiscount = this.recal.data[0].monto_descuento_cliente;
                /* this.items = this.all.data;
                 this.subtotal=this.items[0].subtotal;
                 this.load.dismiss().then(() => {
                     console.log('response success');
                 });*/
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
        });
    }

    nextCheckout(address){
        if(this.delivery == 1){
            this.navCtrl.push(CheckoutConfirmPage,{delivery:this.delivery,cupon:this.cupon})
        }else{
            this.navCtrl.push(CheckoutNextPage,{address:address.id,delivery:this.delivery,cupon:this.cupon})
        }
    }
    sendAddPayment(){
        console.log(this.cards.length);
        if(this.cards.length == 0 && this.delivery == 1){
            this.navCtrl.push(AddCardPage,{param:1,delivery:this.delivery,cupon:this.cupon});
        }else{
            this.navCtrl.push(AddCardPage,{param:2});
        }
    }
    deleteCard(id){
        this.storage.get('token').then((token) => {
            console.log(token);
            let url = URL_SERVICES + "/api/customer/payments/delete_card";
            let headers = new HttpHeaders();
            let js = JSON.stringify({
                "id":id
            });
            //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers = headers.append('Content-Type', 'application/json');
            headers= headers.append('Authorization', 'Bearer '+ token);
            this.http.post(url,js,{headers:headers}).subscribe(response => {
                console.log(response);
                this.response = response;
                if(this.response.ok == true){
                    this.altrCtrl.create({
                        title:"Exito",
                        subTitle:this.response.data,
                        buttons:[
                            {
                                text: 'Entendido',
                                role: 'cancel',
                                handler: () => {
                                    this.getCards();
                                }
                            }
                        ]
                    }).present();
                }else{
                    this.altrCtrl.create({
                        title:this.response.data,
                        subTitle:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                        buttons:[
                            {
                                text: 'Soporte',
                                handler: () => {
                                    console.log('Soporte clicked');
                                    window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                }
                            },
                            {
                                text: 'Entendido',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    }
    getCards(){
        this.storage.get('token').then((token) => {
            console.log(token);
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/payments/get_cards";
                let headers = new HttpHeaders();
                let js = JSON.stringify({
                    "customer":data
                });
                //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    this.res = response;
                    this.cards = this.res.data;
                    console.log(response);
                    this.load.dismiss();
                });
            });
        });
    }

    sendConfirm(card){
      this.navCtrl.push(CheckoutConfirmPage,{address:this.address,payment:card,delivery:this.delivery,cupon:this.cupon, action:3})
    }
}
