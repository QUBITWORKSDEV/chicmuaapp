import { Component } from '@angular/core';
import { NavController, NavParams , AlertController,LoadingController} from 'ionic-angular';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {URL_SERVICES} from "../../config/url.services";
import {Storage} from "@ionic/storage";
import {CheckoutConfirmPage} from "../checkout-confirm/checkout-confirm";
import {CheckoutNextPage} from "../checkout-next/checkout-next";
declare var OpenPay: any;

@Component({
  selector: 'page-add-card',
  templateUrl: 'add-card.html',
})


export class AddCardPage {

    private deviceSessionId: string;
  private numberCard:any;
  private nameProp:any;
  private res:any;
  private type:any = 'undefined';
  private screen:any =1;
  private expiration:any;
  private expiration_m:any;
  private expiration_a:any;
  private cvv:any;
  private state:any;
  private city:any;
  private street:any;
  private colony:any;
  private postal_code:any;
  private vicinity:any;
  private load:any;
  private customerOpen:boolean=false;
  private all:any;
  private page:any;
  private tokenOpen:any;
  private resCards:any;
  private cards:any;
  private delivery:any;
  private cupon:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, private storage: Storage, public altrCtrl:AlertController,public loading:LoadingController) {
      OpenPay.setId('mglzss2usecdekpdud17');
      //OpenPay.setId('m8imutdtb32wp1irrai7');
      OpenPay.setApiKey('pk_cce5b5e14af24a21b7a841702e8040c1');
      //OpenPay.setApiKey('pk_e6cd5a9d2b4845b2aff497dfc779e52a');
      OpenPay.setSandboxMode(false);
      // con esto recuperas el deviceSessionId
      this.deviceSessionId = OpenPay.deviceData.setup('customer-form', 'deviceIdHiddenFieldName');
      console.log('this.deviceSessionId ', this.deviceSessionId );
      this.page = this.navParams.get('param');
      this.delivery = this.navParams.get('delivery');
      this.cupon = this.navParams.get('cupon');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCardPage');
    this.getUserOpenpay();
  }

    getCardType(cardNo) {
      //console.log(cardNo.value);
        var cards = {
            "American Express": /^3[47][0-9]{13}$/,
            "Mastercard": /^5[1-5][0-9]{14}$/,
            "Visa": /^4[0-9]{12}(?:[0-9]{3})?$/
        };

        for(var card in cards) {
            if (cards[card].test(cardNo.value)) {
              //console.log(card);
              this.type = card;
              return card;
            }
        }
       // console.log(undefined);
        this.type = 'undefined';
        //console.log(this.type);
        return undefined;
    }

    next(){
      this.screen = this.screen + 1;
      if(this.screen == 3){
          /*this.altrCtrl.create({
              title:"Recuerda!",
              subTitle:'Recuerda poner la fecha de vencimiento dividiendo el mes y el año con una "/", ejemplo. (06/22)',
              buttons:[
                  {
                      text: 'Entendido',
                      handler: () => {
                      }
                  },
              ]
          }).present();*/
      }
      console.log(this.screen);
    }
    back(){
        this.screen = this.screen - 1;
        console.log(this.screen);
    }

    getUserOpenpay() {
        this.storage.get('token').then((token) => {
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/payments/get_user";
                let headers = new HttpHeaders();
                headers = headers.append('Content-Type', 'application/json');
                headers = headers.append('Authorization', 'Bearer ' + token);
                let js = JSON.stringify({
                    "customer": data
                });
                this.http.post(url, js, {headers: headers}).subscribe(response => {
                    //console.log(response);
                    this.all = response;
                    this.customerOpen = this.all.ok;
                });
            });
        });
    }
    async addCardOpen(){
        console.log('entre aqui ');
        const openpay = await this.extractFormAndCreate();
        console.log('openpay', openpay);
        this.tokenOpen = openpay;
        //alert(this.tokenOpen);
        this.load = this.loading.create({
            content:"Cargando ..."
        });
        this.load.present();
        this.storage.get('token').then((token) => {
            this.storage.get('id').then((data) => {
                //console.log(data);
                let url = URL_SERVICES + "/api/customer/payments/register_card";
                let headers = new HttpHeaders();
                headers = headers.append('Content-Type', 'application/json');
                headers = headers.append('Authorization', 'Bearer ' + token);
                let js = JSON.stringify({
                    "device_session_id":this.deviceSessionId,
                    "token_id":this.tokenOpen,
                    "customer": data
                });
                console.log(js);
                this.http.post(url, js, {headers: headers}).subscribe(response => {
                    console.log(response);
                    this.res = response;
                    if(this.res.ok == true){
                        this.load.dismiss().then(() => {
                        });
                        this.altrCtrl.create({
                            title:"Perfecto!",
                            subTitle:this.res.data,
                            buttons:["Entendido"]
                        }).present();
                        this.getCardsAndPay();
                        this.navCtrl.pop();
                        //this.decision();
                    }else{
                        console.log(this.res);
                        this.altrCtrl.create({
                            title:this.codes(this.res.code),
                            subTitle:"¿Teniendo Problemas? Para soporte marca: 55-3888-1413",
                            buttons:[
                                {
                                    text: 'Soporte',
                                    handler: () => {
                                        console.log('Soporte clicked');
                                        window.open('https://api.whatsapp.com/send?phone=+5215538881413');
                                    }
                                },
                                {
                                    text: 'Entendido',
                                    role: 'cancel',
                                    handler: () => {
                                        console.log('Cancel clicked');
                                    }
                                }
                            ]
                        }).present();
                        this.load.dismiss().then(() => {
                        });
                    }
                },error2 => {
                    console.log(error2);
                });
            });
        });
        //aqui ya hace la llama api de registro que son


    }

    extractFormAndCreate() {
        const openpay = new Promise(
            (resolve, reject) => {
                OpenPay.token.extractFormAndCreate('customer-form', (response) => {
                    resolve(response.data.id);
                }, (response) => {
                    reject(response);
                });
            }
        );
        return openpay;
    }
    codes(code){
        var text;
        switch(code) {
            case 1663: {
                //statements;
                text="La tarjeta fue rechazada";
                break;
            }
            case 1003: {
                //statements;
                text="error en parametros";
                break;
            }
            case 3001: {
                //statements;
                text="La tarjeta fue rechazada";
                break;
            }
            case 3002: {
                //statements;
                text="La tarjeta ha expirado";
                break;
            }

            case 3003: {
                //statements;
                text="La tarjeta no tiene fondos suficientes";
                break;
            }
            case 3004: {
                //statements;
                text="La tarjeta ha sido identificada como una tarjeta robada";
                break;
            }
            case 3005: {
                //statements;
                text="La tarjeta ha sido rechazada por el sistema antifraudes";
                break;
            }
            default: {
                //statements;
                text="Ocurrio algo insesperado";
                break;
            }
        }
        return text;
    }

    getCardsAndPay(){
        this.storage.get('token').then((token) => {
            console.log(token);
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/payments/get_cards";
                let headers = new HttpHeaders();
                let js = JSON.stringify({
                    "customer":data
                });
                //headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    this.resCards = response;
                    this.cards = this.resCards.data;
                    console.log(response);
                    //this.load.dismiss();
                    this.navCtrl.push(CheckoutConfirmPage,{delivery:this.delivery,cupon:this.cupon});
                });
            });
        });
    }

    decision(){
        if(this.delivery == 1){
            this.navCtrl.push(CheckoutConfirmPage,{delivery:this.delivery,cupon:this.cupon});
        }else{
         //   this.navCtrl.push(CheckoutNextPage,{address:address.id,delivery:this.delivery,cupon:this.cupon})
        }
    }
}
