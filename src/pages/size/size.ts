import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {URL_SERVICES} from "../../config/url.services";
import {HelpPage} from "../help/help";
import {AddOtherPage} from "../add-other/add-other";
import {Storage} from "@ionic/storage";
import {CheckoutPage} from "../checkout/checkout";

@Component({
  selector: 'page-size',
  templateUrl: 'size.html',
})
export class SizePage {
  private items:any;
    private js:any;
    private send:any;
    private sendTwo:any;
    private load:any;
    private sizes:any;
    private backgrounds:any;
    private background:any;
    private res:any;
    private url_image:any;
    private all:any;
    private check:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public loading:LoadingController, private storage: Storage) {
      this.js = this.navParams.get("data");
      this.backgrounds = this.navParams.get("backgrounds");
      this.background = this.backgrounds[0].imagensizes;
      this.url_image = URL_SERVICES;
      console.log(this.js);
    this.initialItems();
      this.load = this.loading.create({
          content: 'Cargando...'
      });
      this.load.present();
      this.getSizes();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SizePage');
    //this.getSizes();
  }
    getSizes(){
        let url = URL_SERVICES + "/api/procedures/get_tallas";
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        //headers= headers.append('Authorization', 'Bearer '+data);
        var js = JSON.stringify({
            "uso":this.js.uso
        });
        this.http.post(url,js,{headers:headers}).subscribe(response => {
            //this.restaurants = response;
            //this.items = response;
            this.load.dismiss().then(() => {
                console.log('response success');
            });
            console.log(response);
            this.all = response;
            this.sizes = this.all.data;
        });
    }
    initialItems(){
        this.items = [
            {
                name:'Extra chico',
                sur:'XS',
                id:1,
                img:"../../assets/imgs/xs.png"
            },
            {
                name:'Chico',
                sur:'S',
                id:2,
                img:"../../assets/imgs/s.png"
            },
            {
                name:'Mediano',
                sur:'M',
                id:3,
                img:"../../assets/imgs/m.png"
            },
            {
                name:'Grande',
                sur:'L',
                id:4,
                img:"../../assets/imgs/l.png"
            },
            {
                name:'Extra Grande',
                sur:'XL',
                id:5,
                img:"../../assets/imgs/xl.png"
            },
            {
                name:'Extra Extra Grande',
                sur:'2XL',
                id:6,
                img:"../../assets/imgs/2xl.png"
            },
            {
                name:'Extra Extra Extra Grande',
                sur:'3XL',
                id:7,
                img:"../../assets/imgs/3xl.png"
            }
        ]
    }
    sendHelp(){
      this.navCtrl.push(HelpPage,{data:this.js});
    }
    sendOther(size){
        this.send = {
            "uso":this.js.uso,
            "pattern":this.js.pattern,
            "model":this.js.model,
            "size":size,
            "copas":this.js.copas
        };
        this.navCtrl.push(AddOtherPage, {test:this.send,size:size.talla,backgrounds:this.backgrounds})
    }
    getCheckout(){
        this.storage.get('id_order').then((order)=>{
            console.log(order);
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.sendTwo = {
                "accion":2,
                "orderid":order,
                "cliente":0,
                "shiping":0,
                "coupousoIdnDiscount":0,
                "size":0,
                "patern":0,
                "showpieze":0,
                "use":0,
                "address":0,
                "glasses":0
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                //this.items = response;
                this.res = response;
                this.check = this.res.data.length;
                console.log(this.check.length);
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
        });
    }
    checkout(){
        this.navCtrl.push(CheckoutPage,{action:2});
    }
}
