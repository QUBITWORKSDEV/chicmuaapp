import { Component } from '@angular/core';
import { NavController, NavParams , LoadingController} from 'ionic-angular';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {URL_SERVICES} from "../../config/url.services";

@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {
  private res:any = [];
  private all:any;
  private send:any=[];
  private urlimg:any;
  private load:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public loading:LoadingController) {
      this.send = this.navParams.get("data");
      this.urlimg = URL_SERVICES;
      this.load = this.loading.create({
          content: 'Cargando...'
      });
      this.load.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
    this.getHelp();
  }

    getHelp(){
        let url = URL_SERVICES + "/api/procedures/get_ayuda_tallas";
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        //headers= headers.append('Authorization', 'Bearer '+data);
        var js = {
            tipo:this.send.model.id_tipo
        };
        this.http.post(url,js,{headers:headers}).subscribe(response => {
            //this.restaurants = response;
            //this.items = response;
            this.all = response;
            this.res = this.all.data;
            this.load.dismiss().then(() => {
                console.log('response success');
            });
            console.log(this.res);
        });
    }
}
