import { Component } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {AlertController, App, NavController, NavParams} from 'ionic-angular';

/*---Plugins---*/
import {Storage} from "@ionic/storage";
/*----Pages----*/
import {TabsPage} from "../tabs/tabs";
import {URL_LOGIN, URL_SERVICES} from "../../config/url.services";
import {RegisterPage} from "../register/register";

/*--Provider--*/
import {UsuarioService} from "../../providers/usuario/usuario";
import {CheckoutPage} from "../checkout/checkout";

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    email:string = "";
    pass:string = "";
    token:any;
    page:any;
    line:any;
    send:any;
    remember:boolean = false;
    userData:any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient,private _us: UsuarioService, private storage: Storage,
                private altrCtrl:AlertController, private app:App) {
        this.storage.get('email').then((val) => {
            this.email = val;
        });
        this.storage.get('pass').then((val) => {
            this.pass = val;
        });
        this.page = this.navParams.get("page");
        this.line = this.navParams.get("js");
        console.log(this.page, this.line);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }


    sendRegister(){
        this.navCtrl.push(RegisterPage,{page:2,line:this.line});
    }

    login(){
        this._us.login(this.email,this.pass)
            .subscribe((data)=>{
                    /*   this.alertCtrl.create({
                         title:"Logeado",
                         //subTitle:data,
                         buttons:["OK"]
                       }).present();*/
                    console.log(data);
                    if(this.remember == true){
                        this.storage.set('email', this.email);
                        this.storage.set('pass', this.pass);
                    }else{
                        this.remember = false;
                    }
                    if(this.page != undefined && this.line != undefined){
                        //this.sendOrder();
                        this.app.getRootNav().setRoot(TabsPage,{page:2, line:this.line});
                    }else{
                        this.app.getRootNav().setRoot(TabsPage);
                    }
                },
                (err) => {
                    this.altrCtrl.create({
                        title:"Error",
                        subTitle:"Verifica tu correo y contraseña",
                        buttons:["OK"]
                    }).present();
                }
            )
    }
    /*Enviar la orden que ya se gaurdo por primera vez*/
    sendOrder(){
        this.storage.get('id').then((id)=> {
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":1,
                "orderid":0,
                "cliente":id,
                "shiping":0,
                "coupousoIdnDiscount":0,
                "size":this.line.size.id_talla,
                "patern":this.line.pattern.id_tela,
                "showpieze":this.line.model.id_modelo,
                "use":this.line.uso,
                "address":0,
                "glasses":this.line.copas
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                //this.items = response;
                console.log(response);
                this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
        });
    }

}
