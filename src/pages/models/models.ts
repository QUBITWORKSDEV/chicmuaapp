import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {URL_SERVICES} from "../../config/url.services";
import {ReviewModelPage} from "../review-model/review-model";
import {TipsPage} from "../tips/tips";
import {Storage} from "@ionic/storage";
import {CheckoutPage} from "../checkout/checkout";
import {ClothPage} from "../cloth/cloth";
@Component({
  selector: 'page-models',
  templateUrl: 'models.html',
})
export class ModelsPage {
  private segment:any ='Todos';
  public urlimg:any;
  private use:any;
  private models:any = [];
  private types:any = [];
  private all:any;
  private js:any;
  private load:any;
  private send:any;
  private check:any;
  private res:any;
  private useName:any;
  private backgrounds:any;
  private background:any;
  private imagetip:any;
  private show:boolean = false;
  private text:string = "Lorem ipsum dolor sit amet.";
    private max:any;
    private timer = 0;
    private timerSeg = 0;
    private timerMin = 0;
      constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public loading:LoadingController, private storage: Storage,public toastCtrl: ToastController) {
          this.js = this.navParams.get("data");
          this.use = this.navParams.get("use");
          this.useName = this.navParams.get("use_name");
          this.backgrounds = this.navParams.get("backgrounds");
          this.text = this.navParams.get("text");
          this.background = this.backgrounds[0].imagenmodels;
          this.imagetip  = this.backgrounds[0].imagentip;
          this.max=6000;
          this.urlimg = URL_SERVICES;
          //this.startTimer();
          this.load = this.loading.create({
              content: 'Cargando...'
          });
          this.load.present();
          console.log(this.use);
          console.log(this.backgrounds);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModelsPage');
    //this.initialItems();
    this.getModels();
    this.getTypes();
    this.getCheckout();
    this.presentToast();
  }
    /*startTimer(){
        var intervalVar = setInterval(function () {
            //alert(this.timer++)
            if(this.timer == this.max){
                alert('se cumplio el tiempoooooo!!');
                clearInterval(intervalVar);
            }else{
                if(this.timerSeg == 59){
                    this.timerMin++;
                    this.timerSeg = 0;
                }else{
                    this.timer++;
                    this.timerSeg++;
                }
            }
        }.bind(this),1000)
    }*/

    getModels(){
        console.log(this.js);
        let url = URL_SERVICES + "/api/procedures/get_modelos";
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        this.send = {
            uso:this.use
        };
        //headers= headers.append('Authorization', 'Bearer '+data);
        this.http.post(url,this.send,{headers:headers}).subscribe(response => {
            //this.restaurants = response;
            //this.items = response;
            this.all = response;
            this.models = this.all.data;
            console.log(response);
            this.load.dismiss().then(() => {
                console.log('response success');
            });
        });
    }
    getTypes(){
        console.log(this.js);
        let url = URL_SERVICES + "/api/procedures/get_tipos";
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        this.send = {
            uso:this.use
        };
        //headers= headers.append('Authorization', 'Bearer '+data);
        this.http.post(url,this.send,{headers:headers}).subscribe(response => {
            this.all = response;
            this.types = this.all.data;
            //this.restaurants = response;
            //this.items = response;
            console.log(response);
        });
    }
    log(){
        console.log(this.segment);
    }
    setSegment(type){
        this.segment = type;
        console.log(this.segment);
    }
    sendTips(){
        this.navCtrl.push(TipsPage,{uso:this.use});
    }
    loginState(){
        this.storage.get('id').then((data) => {
            console.log(data);
            if(data != null){
                this.show = true;
            }else{
                this.show= false;
            }
        });
    }
    checkout(){
        this.navCtrl.push(CheckoutPage,{action:2});
    }
    getCheckout(){
        this.storage.get('id_order').then((order)=>{
            console.log(order);
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":2,
                "orderid":order,
                "cliente":0,
                "shiping":0,
                "coupousoIdnDiscount":0,
                "size":0,
                "patern":0,
                "showpieze":0,
                "use":0,
                "address":0,
                "glasses":0,
                "linea":0
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                //this.items = response;
                this.res = response;
                this.check = this.res.data.length;
                console.log(response);
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
        });
    }
    sendCloth(model){
        this.js = {
            use:this.use,
            model:model
        };
        console.log(this.js);
        this.navCtrl.push(ClothPage,{data:this.js,modelName:model.modelo,backgrounds:this.backgrounds});
    }
    presentToast() {
        const toast = this.toastCtrl.create({
            message: 'Elegiste '+this.useName,
            duration: 2000,
            position:'middle',
            cssClass:'toast',
            dismissOnPageChange: true
        });
        toast.present();
    }
}
