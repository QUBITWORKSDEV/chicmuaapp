import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController,ToastController } from 'ionic-angular';
import {ModelsPage} from "../models/models";
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {URL_SERVICES} from "../../config/url.services";
import {ReviewModelPage} from "../review-model/review-model";
import {Storage} from "@ionic/storage";
import {CheckoutPage} from "../checkout/checkout";

@Component({
  selector: 'page-cloth',
  templateUrl: 'cloth.html',
})
export class ClothPage {
  private items:any=[];
  private all:any;
  public urlimg:any;
  private js:any;
  private use:any;
  private load:any;
  private send:any;
  private model:any;
  private backgrounds:any;
  private background:any;
    private max:any;
    private res:any;
    private check:any;
    private timer = 0;
    private timerSeg = 0;
    private timerMin = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public loading:LoadingController,public toastCtrl: ToastController,private storage: Storage) {
    //this.initialItems();
      this.urlimg = URL_SERVICES;
      //this.use = this.navParams.get("use");
      this.js = this.navParams.get("data");
      this.timerSeg = this.navParams.get("seg");
      this.model    = this.navParams.get("modelName");
      this.timerMin = this.navParams.get("min");
      this.backgrounds = this.navParams.get("backgrounds");
      this.background = this.backgrounds[0].imagenpatterns;
      this.max=6000;
      //this.startTimer();
      console.log(this.js);
      this.load = this.loading.create({
          content: 'Cargando...'
      });
      this.load.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClothPage');
    this.getPatterns();
    this.presentToast();
    this.getCheckout();
  }

    startTimer(){
        var intervalVar = setInterval(function () {
            //alert(this.timer++)
            if(this.timer == this.max){
                alert('se cumplio el tiempoooooo!!');
                clearInterval(intervalVar);
            }else{
                if(this.timerSeg == 59){
                    this.timerMin++;
                    this.timerSeg = 0;
                }else{
                    this.timer++;
                    this.timerSeg++;
                }
            }
        }.bind(this),1000)
    }

  getPatterns(){
      let url = URL_SERVICES + "/api/procedures/get_patterns";
      let headers = new HttpHeaders();
      headers = headers.append('Content-Type', 'application/json');
      //headers= headers.append('Authorization', 'Bearer '+data);
      var js = {
          categoria:1,
          use:this.js.use
      };
      console.log(js);
      this.http.post(url,js,{headers:headers}).subscribe(response => {
          //this.restaurants = response;
          this.all = response;
          this.items = this.all.data;
          this.load.dismiss().then(() => {
              console.log('response success');
          });
          console.log(this.items);
      });
  }
    checkout(){
        this.navCtrl.push(CheckoutPage,{action:2});
    }
    getCheckout(){
        this.storage.get('id_order').then((order)=>{
            console.log(order);
            let url = URL_SERVICES + "/api/procedures/set_orders";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            this.send = {
                "accion":2,
                "orderid":order,
                "cliente":0,
                "shiping":0,
                "coupousoIdnDiscount":0,
                "size":0,
                "patern":0,
                "showpieze":0,
                "use":0,
                "address":0,
                "glasses":0
            };
            console.log(this.send);
            //headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,this.send,{headers:headers}).subscribe(response => {
                //this.restaurants = response;
                //this.items = response;
                this.res = response;
                this.check = this.res.data.length;
                console.log(response);
                //this.navCtrl.setRoot(CheckoutPage,{page:2})
            });
        });
    }
  initialItems(){
      this.items = [
          {
              name:'Patron unicornio',
              piece:'10m',
              charge:100,
              ofer:'10%'
          },
          {
              name:'Patron de papaya',
              piece:'10m',
              charge:0,
              ofer:'5%'
          },
          {
              name:'Patron de sandia',
              piece:'10m',
              charge:100,
              ofer:'50%'
          },
          {
              name:'Patron de pera',
              piece:'10m',
              charge:40,
              ofer:'15%'
          }
      ]
  }
    sendReview(pattern){
        this.send = {
            uso:this.js.use,
            pattern:pattern,
            model:this.js.model
        };
        this.navCtrl.push(ReviewModelPage,{data:this.send,tela:pattern.tela,backgrounds:this.backgrounds});
    }
    presentToast() {
        const toast = this.toastCtrl.create({
            message: 'Elegiste '+this.model,
            duration: 2000,
            position:'middle',
            cssClass:'toast',
            dismissOnPageChange: true
        });
        toast.present();
    }
}
