import { Component } from '@angular/core';
import {AlertController, App, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormArray, FormGroup, Validators} from '@angular/forms';
import {AddAddressPage} from "../add-address/add-address";
import {AddCardPage} from "../add-card/add-card";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {URL_SERVICES} from "../../config/url.services";
import {Storage} from "@ionic/storage";
import {TabsPage} from "../tabs/tabs";
import {UsuarioService} from "../../providers/usuario/usuario";
import {TermsPage} from "../terms/terms";
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
    private form: FormGroup;
    private user:any;
    private name:any;
    private firstname:any;
    private secondname:any;
    private birthdate:any;
    private genre:any;
    private email:any;
    private phone:any;
    private all:any;
    private page:any;
    private line:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private _us: UsuarioService,private formBuilder: FormBuilder, private storage: Storage, public http: HttpClient, private altrCtrl:AlertController, private app:App) {
      this.form = this.formBuilder.group({
          name: ['', Validators.required],
          firstname: ['',Validators.required],
          secondname: ['',Validators.required],
          birthdate: ['',Validators.required],
          password: ['',Validators.required],
          genre: ['',Validators.required],
          email: ['',Validators.email],
          phone: [''],
          lada: ['']
      });
      this.page = this.navParams.get("page");
      this.line = this.navParams.get("line");
      console.log(this.page,this.line);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
    logForm(){
            let url = URL_SERVICES + "/api/procedures/set_perfil";
            let headers = new HttpHeaders();
            let js = JSON.stringify({
                "cliente":0,
                "fcmtoken":'',
                "password":this.form.value.password,
                "nombre":this.form.value.name,
                "apepat":this.form.value.firstname,
                "apemat":this.form.value.secondname,
                "email":this.form.value.email,
                "genero":this.form.value.genre,
                "phone":this.form.value.phone,
                "fechnac":this.form.value.birthdate,
            });
            headers = headers.append('Content-Type', 'application/json');
            // headers= headers.append('Authorization', 'Bearer '+ token);
            this.http.post(url,js,{headers:headers}).subscribe(response => {
              this.all = response;
                console.log(response);
                if(this.all.status == true){
                        this._us.login(this.form.value.email,this.form.value.password)
                            .subscribe((data)=>{
                                    /*   this.alertCtrl.create({
                                         title:"Logeado",
                                         //subTitle:data,
                                         buttons:["OK"]
                                       }).present();*/
                                    //this.registerCustomerOpenPay();
                                    if(this.page != undefined && this.line != undefined){
                                        //this.sendOrder();
                                        this.app.getRootNav().setRoot(TabsPage,{page:2, line:this.line});
                                    }else{
                                        this.app.getRootNav().setRoot(TabsPage);
                                    }
                                    this.storage.get('introShown').then((result)=>{
                                        console.log(result);
                                        if(result  == true){

                                        }else{
                                            //this.app.getRootNav().setRoot(TabsPage);
                                            this.storage.set('introShown', true);
                                        }
                                    });
                                },
                                (err) => {
                                    this.altrCtrl.create({
                                        title:"Error",
                                        subTitle:"Verifica tu correo y contraseña",
                                        buttons:["OK"]
                                    }).present();
                                }
                            )
                }else{
                    let alert = this.altrCtrl.create({
                        title:'Error',
                        message: 'Error al registrar',
                        buttons: [
                            {
                                text: 'Entendido',
                                role: 'cancel',
                                handler: () => {
                                    //console.log('Cancel clicked');
                                }
                            }
                        ]
                    });
                    alert.present();
                }
            });
    }
    terms(){
        this.navCtrl.push(TermsPage);
    }
}
