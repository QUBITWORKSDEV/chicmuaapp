import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import {Checkbox, IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

/* Pages */
import { HomePage } from '../pages/home/home';
import {TabsPage} from "../pages/tabs/tabs";
import {ClothPage} from "../pages/cloth/cloth";
import {ModelsPage} from "../pages/models/models";
import {SizePage} from "../pages/size/size";
import {ReviewModelPage} from "../pages/review-model/review-model";
import {HelpPage} from "../pages/help/help";
import {AddOtherPage} from "../pages/add-other/add-other";
import {ProfilePage} from "../pages/profile/profile";
import {VisitPage} from "../pages/visit/visit";
import {CheckoutPage} from "../pages/checkout/checkout";
import {LoginPage} from "../pages/login/login";
import {ThanksPage} from "../pages/thanks/thanks";
import {AddAddressPage} from "../pages/add-address/add-address";
import {AddCardPage} from "../pages/add-card/add-card";
/*Plugins*/
import { HTTP} from "@ionic-native/http";
import { MyApp } from './app.component';
import {RoundProgressModule} from 'angular-svg-round-progressbar';
import {HttpClientModule} from "@angular/common/http";
import {TermsPage} from "../pages/terms/terms";
import {SharePage} from "../pages/share/share";
import { IonicStorageModule } from '@ionic/storage';
import {TipsPage} from "../pages/tips/tips";
import { UsuarioService} from '../providers/usuario/usuario';
import {RegisterPage} from "../pages/register/register";
import {CheckoutNextPage} from "../pages/checkout-next/checkout-next";
import {CheckoutConfirmPage} from "../pages/checkout-confirm/checkout-confirm";
import {PromoPage} from "../pages/promo/promo";
import {SocialSharing} from "@ionic-native/social-sharing";
import { ServicesCardProvider } from '../providers/services-card/services-card';
import { IonInputScrollIntoViewModule } from 'ion-input-scroll-into-view';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,ClothPage,ModelsPage,SizePage,ReviewModelPage,HelpPage,AddOtherPage,ProfilePage,VisitPage,CheckoutPage,LoginPage,ThanksPage,AddAddressPage,AddCardPage,TermsPage,SharePage,TipsPage,RegisterPage,CheckoutNextPage,CheckoutConfirmPage,PromoPage
  ],
  imports: [
    BrowserModule,
    RoundProgressModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    IonInputScrollIntoViewModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,ClothPage,ModelsPage,SizePage,ReviewModelPage,HelpPage,AddOtherPage,ProfilePage,VisitPage,CheckoutPage,LoginPage,ThanksPage,AddAddressPage,AddCardPage,TermsPage,SharePage,TipsPage,RegisterPage,CheckoutConfirmPage,CheckoutNextPage,PromoPage
  ],
  providers: [
    StatusBar,
    HTTP,
    SocialSharing,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsuarioService
  ]
})
export class AppModule {}
